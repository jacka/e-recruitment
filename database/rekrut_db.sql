-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 13 Sep 2019 pada 18.50
-- Versi Server: 10.1.30-MariaDB
-- PHP Version: 5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rekrut_db`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_admin`
--

CREATE TABLE `tbl_admin` (
  `id_admin` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_admin`
--

INSERT INTO `tbl_admin` (`id_admin`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_apply_job`
--

CREATE TABLE `tbl_apply_job` (
  `id_apply_job` int(11) NOT NULL,
  `id_vacancies` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `lulus_test_online` varchar(10) NOT NULL,
  `lulus_interview` varchar(10) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_apply_job`
--

INSERT INTO `tbl_apply_job` (`id_apply_job`, `id_vacancies`, `id_user`, `score`, `lulus_test_online`, `lulus_interview`, `createdAt`) VALUES
(1, 1, 4, 60, 'y', 't', '2019-09-13 15:55:44'),
(2, 1, 3, 80, 'y', 'y', '2019-09-13 16:49:19'),
(3, 1, 10, 35, 't', 't', '2019-09-13 16:00:14');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_family`
--

CREATE TABLE `tbl_family` (
  `id_family` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `placeBirth` varchar(100) NOT NULL,
  `dateBirth` date NOT NULL,
  `status` varchar(50) NOT NULL,
  `lastEducation` varchar(100) NOT NULL,
  `profession` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_formal_education`
--

CREATE TABLE `tbl_formal_education` (
  `id_formal_education` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `educationLevel` varchar(50) NOT NULL,
  `educationInstitution` varchar(100) NOT NULL,
  `certificateSeries` int(11) NOT NULL,
  `yearEntry` int(11) NOT NULL,
  `graduationYear` int(11) NOT NULL,
  `institutionPlace` varchar(100) NOT NULL,
  `certificateNumber` int(11) NOT NULL,
  `imgCertificate` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_formal_education`
--

INSERT INTO `tbl_formal_education` (`id_formal_education`, `id_user`, `educationLevel`, `educationInstitution`, `certificateSeries`, `yearEntry`, `graduationYear`, `institutionPlace`, `certificateNumber`, `imgCertificate`) VALUES
(1, 7, 'SLTA', 'Universitas Gunadarma', 12345, 2016, 2019, 'Jakarta', 2147483647, 'assets/images/certificate/certificate1.jpg'),
(2, 4, 'Sarjana', 'Universitas Gunadarma', 12345, 2017, 2019, 'Jakarta', 2147483647, 'assets/images/certificate/as.PNG'),
(3, 3, 'Magister', 'Universitas Indonesia', 23131, 2016, 2019, 'Jakarta', 12313123, 'assets/images/certificate/certificate2.jpg'),
(5, 10, 'Magister', 'Universitas tertutup', 0, 2001, 2019, 'Jakarta', 2147483647, 'assets/images/certificate/certificate3.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_hasil_jawaban`
--

CREATE TABLE `tbl_hasil_jawaban` (
  `id_hasil_jawaban` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `jawaban` varchar(11) NOT NULL,
  `hasil` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_hasil_jawaban`
--

INSERT INTO `tbl_hasil_jawaban` (`id_hasil_jawaban`, `id_user`, `id_soal`, `jawaban`, `hasil`) VALUES
(1, 3, 1, 'E', 'y'),
(2, 3, 2, 'C', 'y'),
(3, 3, 3, 'C', 'y'),
(4, 3, 4, 'A', 'y'),
(5, 3, 5, 'A', 't'),
(6, 3, 6, 'A', 'y'),
(7, 3, 7, 'C', 'y'),
(8, 3, 8, 'A', 'y'),
(9, 3, 9, 'D', 'y'),
(10, 3, 10, 'A', 'y'),
(11, 3, 11, 'E', 'y'),
(12, 3, 12, 'E', 'y'),
(13, 3, 13, 'B', 'y'),
(14, 3, 14, 'D', 'y'),
(15, 3, 15, 'B', 'y'),
(16, 3, 16, 'B', 't'),
(17, 3, 17, 'C', 'y'),
(18, 3, 18, 'A', 't'),
(19, 3, 19, 'A', 'y'),
(20, 3, 20, 'A', 't'),
(21, 10, 1, 'B', 't'),
(22, 10, 2, 'D', 't'),
(23, 10, 3, 'B', 't'),
(24, 10, 4, 'D', 't'),
(25, 10, 5, 'E', 'y'),
(26, 10, 6, 'E', 't'),
(27, 10, 7, 'E', 't'),
(28, 10, 8, 'E', 't'),
(29, 10, 9, 'E', 't'),
(30, 10, 10, 'D', 't'),
(31, 10, 11, 'E', 'y'),
(32, 10, 12, 'E', 'y'),
(33, 10, 13, 'D', 't'),
(34, 10, 14, 'E', 't'),
(35, 10, 15, 'B', 'y'),
(36, 10, 16, 'E', 't'),
(37, 10, 17, 'C', 'y'),
(38, 10, 18, 'C', 'y'),
(39, 10, 19, 'A', 'y'),
(40, 10, 20, 'A', 't'),
(41, 4, 1, 'E', 'y'),
(42, 4, 2, 'C', 'y'),
(43, 4, 3, 'C', 'y'),
(44, 4, 4, 'A', 'y'),
(45, 4, 5, 'A', 't'),
(46, 4, 6, 'A', 'y'),
(47, 4, 7, 'B', 't'),
(48, 4, 8, 'B', 't'),
(49, 4, 9, 'A', 't'),
(50, 4, 10, 'A', 'y'),
(51, 4, 11, 'C', 't'),
(52, 4, 12, 'A', 't'),
(53, 4, 13, 'B', 'y'),
(54, 4, 14, 'D', 'y'),
(55, 4, 15, 'B', 'y'),
(56, 4, 16, 'B', 't'),
(57, 4, 17, 'C', 'y'),
(58, 4, 18, 'C', 'y'),
(59, 4, 19, 'A', 'y'),
(60, 4, 20, 'A', 't');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_motivation`
--

CREATE TABLE `tbl_motivation` (
  `id_motivation` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `reasonChoose` text NOT NULL,
  `expertise` text NOT NULL,
  `expectedSallary` varchar(100) NOT NULL,
  `hobby` varchar(100) NOT NULL,
  `motivation` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_motivation`
--

INSERT INTO `tbl_motivation` (`id_motivation`, `id_user`, `reasonChoose`, `expertise`, `expectedSallary`, `hobby`, `motivation`) VALUES
(1, 3, 'tes aja dah', 'siap oke', '20000000', 'tester', 'afassadsadasdasdasdadad');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_non_formal_education`
--

CREATE TABLE `tbl_non_formal_education` (
  `id_non_formal_education` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `trainingName` varchar(100) NOT NULL,
  `trainingScope` varchar(100) NOT NULL,
  `trainingInstitution` varchar(100) NOT NULL,
  `certificateNumber` int(11) NOT NULL,
  `startDate` date NOT NULL,
  `endDate` date NOT NULL,
  `imgCertificate` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_non_formal_education`
--

INSERT INTO `tbl_non_formal_education` (`id_non_formal_education`, `id_user`, `trainingName`, `trainingScope`, `trainingInstitution`, `certificateNumber`, `startDate`, `endDate`, `imgCertificate`) VALUES
(1, 3, 'Junior Programming', 'National', 'Kominfo', 2147483647, '2019-09-05', '2019-09-11', 'assets/images/certificate/certificate22.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_personal_data`
--

CREATE TABLE `tbl_personal_data` (
  `id_personal_data` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `fullName` varchar(50) NOT NULL,
  `placeBirth` varchar(50) NOT NULL,
  `dateBirth` date NOT NULL,
  `gender` varchar(10) NOT NULL,
  `height` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `religion` varchar(20) NOT NULL,
  `phoneNumber` varchar(20) NOT NULL,
  `address` text NOT NULL,
  `gambarProfil` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_personal_data`
--

INSERT INTO `tbl_personal_data` (`id_personal_data`, `id_user`, `fullName`, `placeBirth`, `dateBirth`, `gender`, `height`, `weight`, `religion`, `phoneNumber`, `address`, `gambarProfil`) VALUES
(1, 4, 'Jaka Pranata Kusuma', 'Bandung', '1994-05-27', 'Male', 160, 60, 'Islam', '085861752536', 'Jl. H Ijo RT/RW : 009/013 Kel. Kapuk Kec.Cengkareng', 'assets/images/profile/NLP.jpg'),
(2, 3, 'Joko Santoso', 'Jakarta Barat', '2019-09-18', 'Male', 170, 70, 'islam', '85861752536', 'Ruko SOHO Citra 8 Blok D3 No3 Pegadungan Kalideres Jakarta Barat 11830', 'assets/images/profile/cari-kerja.jpg'),
(3, 10, 'Nurul Apipah', 'Tasikmalaya', '1994-07-17', 'Female', 150, 55, 'islam', '85861752536', 'Jl. H Ijo RT/RW : 009/013 Kel. Kapuk Kec.Cengkareng', 'assets/images/profile/res.PNG');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_pilihan_jawaban`
--

CREATE TABLE `tbl_pilihan_jawaban` (
  `id_pilihan_jawaban` int(11) NOT NULL,
  `id_soal` int(11) NOT NULL,
  `pilihan_a` text NOT NULL,
  `pilihan_b` text NOT NULL,
  `pilihan_c` text NOT NULL,
  `pilihan_d` text NOT NULL,
  `pilihan_e` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_pilihan_jawaban`
--

INSERT INTO `tbl_pilihan_jawaban` (`id_pilihan_jawaban`, `id_soal`, `pilihan_a`, `pilihan_b`, `pilihan_c`, `pilihan_d`, `pilihan_e`) VALUES
(2, 1, 'Bachroedin Jusuf Habibie', 'Megawati Soekaro Putri', 'Ir. H Soekarno', 'Ir. H Djuanda', 'Jendral Soeharto'),
(3, 2, ' Bahasa mesin\r\n', ' Bahasa e-mail\r\n', 'Bahasa HTML\r\n', 'Bahasa Pascal\r\n', 'Bahasa Komputer'),
(4, 3, ' Keyboard\r\n\r\n', 'Monitor\r\n', 'Scanner\r\n', ' Modem\r\n', 'Mouse'),
(5, 4, 'System operasi\r\n', 'Program utilitas\r\n', 'Program aplikasi\r\n', 'Program paket\r\n', 'Bahasa pemrograman'),
(7, 5, 'UNIX\r\n', 'MS MICROSOFT\r\n', 'BIOS\r\n', 'MS WINDOWS\r\n', 'MS OFFICE'),
(8, 6, '.dat\r\n', '*.htm\r\n', '*.Ftp\r\n', '*.doc\r\n', ' 8.nml'),
(9, 7, 'Grafis bitmap dan raster\r\n', 'Grafis vector dan raster\r\n', 'Grafis raster dan grafis resolusi\r\n', 'Grafis bitmap dan grafis vector\r\n', 'Garis dan kurva'),
(10, 8, 'Akan terlihat pecah atau kabur\r\n', 'Akan terlihat makin jelas\r\n', 'Memiliki kemampuan untuk tidak pecah\r\n', 'Gradasi akan berkurang\r\n', 'Detail warna hilang'),
(11, 9, 'Tool zoom\r\n', 'Tool pick\r\n', 'Tool polygon\r\n', 'Tool rectangle\r\n', 'Tool ellips'),
(12, 10, 'Mengatur tampilan\r\n', 'Mengisi bidang dengan pilihan warna tertentu atau obyek isian tertentu\r\n', 'Memindahkan obyek\r\n', 'Membesarkan dan mengecilkan\r\n', 'Membuat tulisan yang sesuai dengan tulisan'),
(13, 11, 'Klik edit pilih properties\r\n', 'Klik edit pilih paste\r\n', 'Klik file pilih save\r\n', 'Klik file pilih new\r\n', 'Klik file pilih open'),
(14, 12, 'Sonique\r\n', 'Winamp\r\n', 'Xing\r\n', 'Cowon Jet Audio\r\n', 'Corel Photo Paint'),
(15, 13, 'Menghapus File\r\n', 'Membuat duplikat file\r\n', 'Memainkan file dalam album management\r\n', 'Mengurutkan file\r\n', 'Mencari file'),
(16, 14, 'Backspace\r\n', 'Page Up\r\n', 'Alt\r\n', 'Page Down\r\n', 'Shift'),
(17, 15, 'By file name\r\n', 'By file extension\r\n', 'By description\r\n', 'By folder\r\n', 'Shuffle'),
(18, 16, 'Format > side design dari menu bar > pilih template > dan klik apply\r\n', 'Format > slide layout dari menu bar, pilih layout yang diinginkan dan klik apply\r\n', 'Slide show > hide slide\r\n', 'Slide show > View show > klik next\r\n', 'Insert > slide number > klik from file'),
(19, 17, 'Bahasa mesin\r\n', 'Bahasa e-mail\r\n', 'Bahasa HTML\r\n', 'Bahasa Pascal\r\n', 'Bahasa Komputer'),
(20, 18, 'Keyboard\r\n', 'Monitor\r\n', 'Scanner\r\n', 'Modem\r\n', 'Mouse'),
(21, 19, '1), 2) dan 3)', '1), 2) dan 4)\r\n\r\n', '2), 3) dan 4)\r\n\r\n', '2), 4) dan 5)\r\n\r\n', '3), 4) dan 5)'),
(22, 20, 'Bank Indonesia\r\n\r\n', 'bank umum milik swasta asing\r\n\r\n', 'bank koperasi\r\n\r\n', 'bank perkreditan\r\n\r\n', 'bank daerah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_soal`
--

CREATE TABLE `tbl_soal` (
  `id_soal` int(11) NOT NULL,
  `soal` text NOT NULL,
  `jawaban` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_soal`
--

INSERT INTO `tbl_soal` (`id_soal`, `soal`, `jawaban`) VALUES
(1, 'Siapakah presiden Kedua RI ?', 'E'),
(2, 'Bahasa yang digunakan secara default pada aplikasi Front Page adalah', 'C'),
(3, 'Berikut ini yang termasuk perangkat input sekaligus output pada komputer adalah', 'C'),
(4, 'Perangkat lunak komputer yang berfungsi sebagai perangkat pemelihara komputer seperti anti-virus, partisi harddisk dan lain-lain termasuk klasifikasi', 'A'),
(5, 'Salah satu system operasi (operating system) yang mulai banyak digunakan karena merupakan Open Source dan dapat diperoleh tanpa membayar (tinggal download di internet) adalah ….', 'E'),
(6, 'Tampilan halaman pada aplikasi Front Page dengan posisi normal memiliki ekstension ….', 'A'),
(7, 'Secara garis besar, desain grafis terbagi menjadi 2 jenis yaitu….', 'C'),
(8, 'Apabila image bitmap diperbesar melebihi kemampuan maksimalnya, maka hasilnya adalah ….', 'A'),
(9, 'Untuk membesarkan dan mengecilkan tampilan, tool yang dipergunakan adalah ….', 'D'),
(10, 'Fill tool adalah alat dari program aplikasi CorelDRAW yang dipergunakan untuk ….', 'A'),
(11, 'Setiap pekerjaan yang telah kita buat tentu harus disimpan. Cara menyimpan hasil kerja dimulai dengan ….', 'E'),
(12, 'Program aplikasi untuk menjalankan editing suara, gambar, ataupun video banyak sekali diantaranya berikut ini, kecuali….', 'E'),
(13, 'Menu Tool, kemudian pilih file copy adalah perintah untuk ….', 'B'),
(14, 'Untuk berpindah slide berikutnya dalam melakukan presentasi tombol yang digunakan adalah ….', 'D'),
(15, 'Untuk melakukan pengurutan sesuai dengan nama extensi file, maka harus dipilih …', 'B'),
(16, 'Untuk mengubah tampilan tata letak slide, langkahnya adalah ….\r\n', 'A'),
(17, 'Bahasa yang digunakan secara default pada aplikasi Front Page adalah ….', 'C'),
(18, 'Berikut ini yang termasuk perangkat input sekaligus output pada komputer adalah ….', 'C'),
(19, 'Fungsi dari bank adalah sebagai berikut:\r\n\r\n1) Menyediakan jasa perbankan.\r\n\r\n2) Menghimpun dana dari masyarakat dalam bentuk simpanan.\r\n\r\n3) Mengatur dan menjaga sistem pembayaran.\r\n\r\n4) Memberi pinjaman atau kredit kepada masyarakat.\r\n\r\n5) Mengatur dan mengawasi bank di Indonesia.\r\n\r\nPernyataan yang merupakan fungsi dari bank umum adalah ....', 'A'),
(20, 'Untuk mengeluarkan dan mengedarkan uang rupiah serta mencabut, menarik dan memusnahkan dari peredaran merupakan tugas wewenang dari ....', 'D');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id_user` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_users`
--

INSERT INTO `tbl_users` (`id_user`, `username`, `email`, `password`, `status`) VALUES
(3, 'Jaka Permana', 'jackapermana@gmail.com', '78f3197f175850d13057dc8dfc59b912', 'verified'),
(4, 'Jaya Pranata', 'jackapermana@yahoo.com', '78f3197f175850d13057dc8dfc59b912', 'verified'),
(8, 'ter', 'cs.recruitment1223@gmail.com', '056f32ee5cf49404607e368bd8d3f2af', 'unverified'),
(9, 'tester', 'cs.recruitment123@gmail.com', '8e607a4752fa2e59413e5790536f2b42', 'verified'),
(10, 'Nurul Apipah', 'nurulafifah170@gmail.com', '55811d685377dc59c4f23b946670dcca', 'verified'),
(11, 'tes', 'jaka@jaka.com', '78f3197f175850d13057dc8dfc59b912', 'verified');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_vacancies`
--

CREATE TABLE `tbl_vacancies` (
  `id_vacancies` int(11) NOT NULL,
  `unit` varchar(200) NOT NULL,
  `position` varchar(200) NOT NULL,
  `sub_unit` varchar(200) NOT NULL,
  `deadline` date NOT NULL,
  `location` varchar(200) NOT NULL,
  `quantity` int(11) NOT NULL,
  `type` varchar(200) NOT NULL,
  `salary` varchar(200) NOT NULL,
  `level` varchar(200) NOT NULL,
  `education` varchar(200) NOT NULL,
  `responsibillities` text NOT NULL,
  `requirements` text NOT NULL,
  `experience` varchar(10) NOT NULL,
  `status` varchar(100) NOT NULL,
  `action` varchar(100) NOT NULL,
  `createdBy` varchar(100) NOT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_vacancies`
--

INSERT INTO `tbl_vacancies` (`id_vacancies`, `unit`, `position`, `sub_unit`, `deadline`, `location`, `quantity`, `type`, `salary`, `level`, `education`, `responsibillities`, `requirements`, `experience`, `status`, `action`, `createdBy`, `createdAt`) VALUES
(1, 'IT/Computer - Software', 'Senior Programmer', 'Data Scientist', '2019-09-16', 'Jakarta Pusat', 1, 'Full-Time', '1000000', 'CEO / GM / Director / Senior Manager', 'SLTA', 'mencari pekerjaan baru', 'tes', 'true', 'done', 'done', 'admin', '2019-09-13 13:24:50');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tbl_work_experience`
--

CREATE TABLE `tbl_work_experience` (
  `id_work_experience` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `institutionName` varchar(100) NOT NULL,
  `position` varchar(100) NOT NULL,
  `startDate` varchar(100) NOT NULL,
  `endDate` varchar(100) NOT NULL,
  `jobDescription` text NOT NULL,
  `reasonResign` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tbl_work_experience`
--

INSERT INTO `tbl_work_experience` (`id_work_experience`, `id_user`, `institutionName`, `position`, `startDate`, `endDate`, `jobDescription`, `reasonResign`) VALUES
(1, 4, 'Pinopi', 'Programmer', '2017', '2019', 'programmer', 'pengen keluar dari zona nyaman hehe');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `tbl_apply_job`
--
ALTER TABLE `tbl_apply_job`
  ADD PRIMARY KEY (`id_apply_job`);

--
-- Indexes for table `tbl_family`
--
ALTER TABLE `tbl_family`
  ADD PRIMARY KEY (`id_family`);

--
-- Indexes for table `tbl_formal_education`
--
ALTER TABLE `tbl_formal_education`
  ADD PRIMARY KEY (`id_formal_education`);

--
-- Indexes for table `tbl_hasil_jawaban`
--
ALTER TABLE `tbl_hasil_jawaban`
  ADD PRIMARY KEY (`id_hasil_jawaban`);

--
-- Indexes for table `tbl_motivation`
--
ALTER TABLE `tbl_motivation`
  ADD PRIMARY KEY (`id_motivation`);

--
-- Indexes for table `tbl_non_formal_education`
--
ALTER TABLE `tbl_non_formal_education`
  ADD PRIMARY KEY (`id_non_formal_education`);

--
-- Indexes for table `tbl_personal_data`
--
ALTER TABLE `tbl_personal_data`
  ADD PRIMARY KEY (`id_personal_data`);

--
-- Indexes for table `tbl_pilihan_jawaban`
--
ALTER TABLE `tbl_pilihan_jawaban`
  ADD PRIMARY KEY (`id_pilihan_jawaban`);

--
-- Indexes for table `tbl_soal`
--
ALTER TABLE `tbl_soal`
  ADD PRIMARY KEY (`id_soal`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tbl_vacancies`
--
ALTER TABLE `tbl_vacancies`
  ADD PRIMARY KEY (`id_vacancies`);

--
-- Indexes for table `tbl_work_experience`
--
ALTER TABLE `tbl_work_experience`
  ADD PRIMARY KEY (`id_work_experience`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_admin`
--
ALTER TABLE `tbl_admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_apply_job`
--
ALTER TABLE `tbl_apply_job`
  MODIFY `id_apply_job` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_family`
--
ALTER TABLE `tbl_family`
  MODIFY `id_family` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_formal_education`
--
ALTER TABLE `tbl_formal_education`
  MODIFY `id_formal_education` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tbl_hasil_jawaban`
--
ALTER TABLE `tbl_hasil_jawaban`
  MODIFY `id_hasil_jawaban` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT for table `tbl_motivation`
--
ALTER TABLE `tbl_motivation`
  MODIFY `id_motivation` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_non_formal_education`
--
ALTER TABLE `tbl_non_formal_education`
  MODIFY `id_non_formal_education` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_personal_data`
--
ALTER TABLE `tbl_personal_data`
  MODIFY `id_personal_data` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_pilihan_jawaban`
--
ALTER TABLE `tbl_pilihan_jawaban`
  MODIFY `id_pilihan_jawaban` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `tbl_soal`
--
ALTER TABLE `tbl_soal`
  MODIFY `id_soal` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_vacancies`
--
ALTER TABLE `tbl_vacancies`
  MODIFY `id_vacancies` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_work_experience`
--
ALTER TABLE `tbl_work_experience`
  MODIFY `id_work_experience` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
