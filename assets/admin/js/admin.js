$(function() {
	// tombol alert

  //khusus job vacancy
	$(document).ready(function() {
		let flash = $(".flash-data").attr("data-vacancy");
		if (flash && flash != undefined && flash == "add") {
			Swal({
				title: "Berhasil",
				text:
					"data berhasil ditambahkan",
				type: "success"
			});
		}else if (flash && flash != undefined && flash == "update") {
			Swal({
				title: "Berhasil",
				text:"data berhasil diperbaharui",
				type: "success"
			});
		}else if (flash && flash != undefined && flash == "over") {
			Swal({
				title: "Perhatian",
				text:"Jumlah pelamar yang diterima melebihi Qty yang ditetapkan!",
				type: "warning"
			});
		}else if (flash && flash != undefined && flash == "minus") {
			Swal({
				title: "Perhatian",
				text:"harap Acc pelamar minimal 1",
				type: "warning"
			});
		}
  });
  
	//khusus register user berhasil
	$(document).ready(function() {
		let flash = $(".flash-data").attr("data-register");
		console.log("woy", flash);
		if (flash && flash != undefined && flash == "berhasil") {
			Swal({
				title: "Berhasil",
				text:
					"Akun telah berhasil didaftarkan harap buka email untuk memverfikasi akun anda",
				type: "success"
			});
		}
	});

	//apabila sudah terdaftar
	$(document).ready(function() {
		let flash = $(".flash-data").attr("data-register");
		console.log("woy", flash);
		if (flash && flash != undefined && flash == "terdaftar") {
			Swal({
				title: "Perhatian",
				text: "Akun sudah terdaftar",
				type: "error"
			});
		}
	});

	//khusus register login user gagal
	$(document).ready(function() {
		let flash = $(".flash-data").attr("data-login-user");
		console.log("woy", flash);
		if (flash && flash != undefined && flash == "gagal") {
			Swal({
				title: "Gagal",
				text: "Akun anda belum terdaftar",
				type: "error"
			});
		}else if (flash && flash != undefined && flash == "unmatch") {
			Swal({
				title: "Gagal",
				text: "Password anda tidak cocok",
				type: "error"
			});
		}else if (flash && flash != undefined && flash == "update") {
			Swal({
				title: "Berhasil",
				text: "Password anda berhasil diubah",
				type: "success"
			});
		}
		
	});

	$(document).ready(function() {
		let flash = $(".flash-data").attr("data-login-user");
		console.log("woy", flash);
		if (flash && flash != undefined && flash == "unverified") {
			Swal({
				title: "unverified",
				text: "Akun anda belum terverifikasi",
				type: "error"
			});
		}
	});

	// khusus login gagal

	$(document).ready(function() {
		let flash = $(".flash-data").attr("data-login");
		if (flash && flash != undefined && flash == "gagal") {
			Swal({
				title: "data tidak ditemukan",
				text: "Admin belum terdaftar",
				type: "error"
			});
		}
	});

	// tutup login gagal

	// flash alert
	$(document).ready(function() {
		let flash = $(".flash-data").attr("data-flashdata");
		if (flash && flash != undefined && flash == "berhasil") {
			Swal({
				title: "Sukses",
				text: "Perbaharui " + flash,
				type: "success"
			});
		} else if (flash && flash != undefined) {
			Swal({
				title: flash,
				text: "Terjadi Kesalahan Jaringan",
				type: "error"
			});
		}
	});

	// tutup alert

	// flash alert pembayaran
	$(document).ready(function() {
		let flash = $(".flash-bayar").attr("data-flashdata");

		console.log("bayar =>", flash);
		if (flash && flash != undefined && flash == "berhasil") {
			Swal({
				title: "Sukses",
				text: "Perbaharui " + flash,
				type: "success"
			});
		} else if (flash && flash != undefined && flash == "kosong") {
			Swal({
				title: flash,
				text: "Harap Isi No Telepon",
				type: "error"
			});
		} else if (flash && flash != undefined && flash == "gagal") {
			Swal({
				title: "Gagal",
				text: "Update Data Gagal",
				type: "error"
			});
		}
	});

	// tutup alert pembayaran

	//   // flash alert lunas
	//   $(document).ready(function(){
	//     let flash = $('.flash-lunas').attr('data-lunas');

	//     console.log("bayar =>", flash)
	//    if( flash && flash != undefined && flash == "berhasil"){
	//       Swal({
	//         title : "Sukses",
	//         text  : "Pembayaran Lunas",
	//         type  : 'success'
	//       });
	//         }
	//       else if(flash && flash != undefined && flash == "gagal"){
	//           Swal({
	//             title : "Gagal",
	//             text  : "Pembayaran Gagal",
	//             type  : 'error'
	//           });
	//         }
	//       });

	// // tutup alert lunas

	$(".tombol-batal").on("click", function(event) {
		event.preventDefault();
		console.log("tusbol");

		Swal.fire({
			title: "Batal ?",
			text: "Apakah Kamu Yakin !",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#3085d6",
			cancelButtonColor: "#d33",
			cancelButtonText: "Tidak",
			confirmButtonText: "Ya, Batalkan!"
		}).then(result => {
			if (result.value) {
				// document.location.href = "http://localhost/family/admin/daftarAlumni"
			}
		});
	});

	$(".ubah-no-hp").on("click", function(event) {
		event.preventDefault();
		console.log("cok");

		$("#ubahNoHp").modal({
			backdrop: "static"
		});

		let id = $(this).attr("id");
		let nama = $(this).attr("nama");
		let kelamin = $(this).attr("kelamin");
		let ttl = $(this).attr("ttl");
		let alamat = $(this).attr("alamat");
		let jurusan = $(this).attr("jurusan");
		let lulusan = $(this).attr("lulusan");
		let angkatan = $(this).attr("angkatan");

		console.log(nama);

		$("#id").val(id);
		$("#nama").val(nama);
		$("#kelamin").val(kelamin);
		$("#ttl").val(ttl);
		$("#alamat").val(alamat);
		$("#jurusan").val(jurusan);
		$("#lulusan").val(lulusan);
		$("#angkatan").val(angkatan);
	});

	$(".tombol-detail").on("click", function(event) {
		event.preventDefault();

		console.log("tercyduk");
		$("#lihatDetail").modal({
			backdrop: "static"
		});

		let nama = $(this).attr("nama");
		let kelamin = $(this).attr("kelamin");
		let ttl = $(this).attr("ttl");
		let alamat = $(this).attr("alamat");
		let jurusan = $(this).attr("jurusan");
		let lulusan = $(this).attr("lulusan");
		let angkatan = $(this).attr("angkatan");

		console.log(nama);

		$("#nama").html(nama);
		$("#kelamin").html(kelamin);
		$("#ttl").html(ttl);
		$("#alamat").html(alamat);
		$("#jurusan").html(jurusan);
		$("#lulusan").html(lulusan);
		$("#angkatan").html(angkatan);
		// Swal({
		//     title : "Data Anda",
		//     text  : "Berhasil " ,
		//     type  : 'success'
		//   });
	});

	$(".tombol-bayar").on("click", function(event) {
		event.preventDefault();

		console.log("wer", "/");

		let nama = $(this).attr("nama");

		let id = $(this).attr("id");

		Swal.fire({
			title: "Konfirmasi ?",
			text: "Apakah " + nama + " sudah membayar ?",
			type: "info",
			showCloseButton: true,
			showCancelButton: false,
			confirmButtonColor: "#3085d6",
			confirmButtonText: "Ya, Lunas !"
		}).then(result => {
			if (result.value) {
				$.ajax({
					type: "POST",
					data: { id_user: id },
					url: "/admin/bayarLunas",
					success: function(data) {
						console.log("Berhasil", data);

						Swal({
							title: "Suksea",
							text: "Pembayaran Lunas",
							type: "success"
						}).then(result => {
							if (result.value) {
								location.reload();
							}
						});
					},
					error: function(data) {
						console.log("error", data);
						Swal({
							title: "Gagal",
							text: "Gagal Merubah Status Pembayaran",
							type: "error"
						});
					}
				});

				// document.location.href = "http://localhost/family/admin/daftarAlumni"
			}
		});
	});
	// $(document).ready(function() {
	//     var table =  $('.mydatatable').DataTable();

	//     $('.mydatatable tbody').on('click', 'tr', function () {
	//         let data = table.row( this ).data();
	//         // alert( 'You clicked on '+data[1]+'\'s row' );
	//          Swal("Ini data alumni" + data[1]);
	//     } );
	// } );
});
