<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function index()
	{
    if(!$this->session->userdata('username')){
      redirect('admin/login');
    }
    
    $data['total_lulus_interview'] = $this->admin_models->getLulusTestInterview()->num_rows();
    $data['total_lulus_test_online'] = $this->admin_models->getLulusTestOnline()->num_rows();
    $data['total_apply'] = $this->admin_models->getTotalApplicant()->num_rows();
    $this->load->view('admin/template/header');
    $this->load->view('admin/home', $data);
    $this->load->view('admin/template/footer');

    }

    public function listJob()
	{
    if(!$this->session->userdata('username')){
      redirect('admin/login');
    }
    
    $data['vacancies'] = $this->admin_models->getVacancies();
    $this->load->view('admin/template/header');
    $this->load->view('admin/listJob', $data);
    $this->load->view('admin/template/footer');

    }

    public function listApplicant()
	{
    if(!$this->session->userdata('username')){
      redirect('admin/login');
    }
    
    $data['vacancies'] = $this->admin_models->getVacancies();
    $data['applicant'] = $this->admin_models->getAllApplicant();

    $this->load->view('admin/template/header');
    $this->load->view('admin/listApplicant', $data);
    $this->load->view('admin/template/footer');

    }

    public function applicantByJob($idJob)
	{
    if(!$this->session->userdata('username')){
      redirect('admin/login');
    }
    
    $data['vacancies'] = $this->admin_models->getVacancies();
    $data['applicant'] = $this->admin_models->getApplicant($idJob);

    $this->load->view('admin/template/header');
    $this->load->view('admin/applicantByJob', $data);
    $this->load->view('admin/template/footer');

    }

    public function detailApplicantByJob($idJob)
	{
    if(!$this->session->userdata('username')){
      redirect('admin/login');
    }
    
    $data['vacancies'] = $this->admin_models->getVacancies();
    $data['applicant'] = $this->admin_models->getApplicant($idJob);

    $this->load->view('admin/template/header');
    $this->load->view('admin/detailApplicantByJob', $data);
    $this->load->view('admin/template/footerScroll');

    }


    public function listReport()
	{
    if(!$this->session->userdata('username')){
      redirect('admin/login');
    }
    
    $data['vacancies'] = $this->admin_models->getVacancies();

    $this->load->view('admin/template/header');
    $this->load->view('admin/listReport', $data);
    $this->load->view('admin/template/footer');

    }

    public function testOnline()
	{
    if(!$this->session->userdata('username')){
      redirect('admin/login');
    }
    
    $data['test_online'] = $this->user_models->getTestGeneralWithoutId();
    
    $this->load->view('admin/template/header');
    $this->load->view('admin/testOnline', $data);
    $this->load->view('admin/template/footer');
    }

    public function inputSchedule($idVacancy)
	{
    if(!$this->session->userdata('username')){
      redirect('admin/login');
    }
    
    if(isset($_POST['submit'])){
      if(!empty($this->input->post('schedule'))){
        $dataArray = $this->input->post("email");
        $date = $this->input->post("schedule");
        $formatDate = date("d-m-Y", strtotime($date));

        $this->load->library('phpmailer_lib');

              $mail = $this->phpmailer_lib->load();
      
              $mail->isSMTP();
              $mail->Host = 'smtp.gmail.com';
              $mail->SMTPAuth = true;
              $mail->Username = 'cs.recruitment123@gmail.com';
              $mail->Password = 'rekrut123';
              $mail->SMTPSecure = 'tls';
              $mail->Port = 587;
      
              $mail->setFrom('cs.recruitment123@gmail.com', 'Recruitment');
              $mail->addReplyTo('cs.recruitment123@gmail.com', 'Recruitment');
      
              // $mail->addAddress($emailUser);
              // $mail->AddAddress('person2@domain.com', 'Person Two');
              foreach($dataArray as $emailAddress){
                $mail->AddAddress(trim($emailAddress));
                }
      
              $mail->Subject = "Recruitment";
      
              $mail->isHtml(true);
      
              $mailContent = "<html>
              <head>
                  <meta charset='utf-8'>
                  <meta http-equiv='X-UA-Compatible' content='IE=edge'>
                  <title>E-Recruitment | Undangan Interview</title>
                  <meta name='viewport content='width=device-width, initial-scale=1'>
              </head>
              
              <body style'background: #eaeaea; font-size: 15px; font-family:Arial;'>
              <table cellspacing='0'
                    style='margin: 0 auto; width: 600px; border: 1px solid #ccc; border-radius: 5px; background: #FFF'>
                  <tr>
                      <td style='padding: 15px 50px; width: 50%;'>
                          <h3>Recruitment Invitation</h3>
                      </td>
                  </tr>
                  <tr>
                      <td colspan='2' style='font-family:Arial; padding: 0 50px;'>
                      In response to your submitted application, I would to invite you to our interview on :
                      </td>
                  </tr>
                  <tr>
                      <td style='padding: 18px 50px 18px 50px;' colspan='2'>
                          <div style='background: #eaeaea; padding: 20px;'>
                              <div style='text-align: center;'>Date</div>
                              <div style='text-align: center;'>$formatDate</div>
                          </div>
                      </td>
                  </tr>

                  <tr>
                  <td style='padding: 18px 50px 18px 50px;' colspan='2'>
                      <div style='background: #eaeaea; padding: 20px;'>
                          <div style='text-align: center;'>Location</div>
                          <div style='text-align: center;'>PT Megah Mas Prima Tangerang</div>
                      </div>
                  </td>
              </tr>
              
                  <tr>
                  <td colspan='2' style='font-family:Arial; padding: 50px 50px;'>
                            The interview will last about 30 minutes or less. Please bring your curriculum vittae, identity card and real supporting document that you had submitted on our web.

                            if you can't follow the interview at the time specified, please contact us by phone (021-5960720).

                            Thank you.
                            PT. Megah Mas Prima

                  </td>
                </tr>
                
                  <tr style='background: #1eadff;'>
                      <td colspan='2'>
                          <div>
                              <div style='margin-top: 30px;'>
                                  <p style='font-family:Arial; text-align: center; color: #FFF'>Follow Us</p>
                              </div>
                          </div>
                      </td>
                  </tr>
                  <tr style='background: #1eadff;'>
                      <td colspan='2'>
                          <div style='text-align: center;padding:50px'>
                                  <span>
                                      <a href='https://www.facebook.com/pinopi.official' target='_blank'>
                                          <img src='http://pembalutsani.com/assets/user/images/v2/home/email/fb.png' width='40' />
                                      </a>
                                  </span>
                              <span style='margin: 0 20px;'>
                                      <a href='https://www.instagram.com/pinopi.official/'>
                                          <img src='http://pembalutsani.com/assets/user/images/v2/home/email/yt.png' width='40' />
                                      </a>
                                  </span>
                              <span>
                                      <a href='https://www.youtube.com/c/pinopi'>
                                          <img src='http://pembalutsani.com/assets/user/images/v2/home/email/ig.png' width='40' />
                                      </a>
                                  </span>
                          </div>
                      </td>
                  </tr>
                </table>
              </body>
              
              </html>";

      $mail->Body = $mailContent;
          
      if(!$mail->send()){
          echo 'Pesan Berhasil gagal dikirim';
          echo 'Pesan Error : ' . $mail->ErrorInfo;
      }else{
        $dataVacancy = $this->admin_models->getVacanciesById($idVacancy)->row();

        $dataArray = array(
          'unit' => $dataVacancy->unit,
          'position' => $dataVacancy->position,
          'sub_unit' => $dataVacancy->sub_unit,
          'deadline' => $dataVacancy->deadline,
          'location' => $dataVacancy->location,
          'quantity' => $dataVacancy->quantity,
          'type' => $dataVacancy->type,
          'salary' => $dataVacancy->salary,
          'level' => $dataVacancy->level,
          'education' => $dataVacancy->education,
          'responsibillities' => $dataVacancy->responsibillities,
          'requirements' => $dataVacancy->requirements,
          'experience' => $dataVacancy->experience,
          'status' => "interview",
          'action' => "input_result_interview",
          'createdBy' => $this->session->userdata('username'),
          );
  
           $hasil = $this->admin_models->updateJobVacancy($idVacancy, $dataArray);
           $this->session->set_flashdata('item', 'update');
           redirect(base_url('admin/listJob'));
      }

      }
    }else{
      $data['applicant'] = $this->admin_models->getApplicant($idVacancy);
      $data['vacancy'] = $this->admin_models->getVacanciesById($idVacancy);
      $this->load->view('admin/template/header');
      $this->load->view('admin/inputSchedule', $data);
      $this->load->view('admin/template/footer');
    }
    }

    public function resultInterview($idVacancy)
	{
      if(!$this->session->userdata('username')){
        redirect('admin/login');
      }
      
      if(isset($_POST['submit'])){
       
     

        if( count($this->input->post('check')) > intval($this->input->post('qty')  ) ){
            $this->session->set_flashdata('item', 'over');
            redirect(base_url("admin/resultInterview/$idVacancy"));
        }else if(count($this->input->post('check')) < 1){
            $this->session->set_flashdata('item', 'minus');
            redirect(base_url("admin/resultInterview/$idVacancy"));
        }else{
            $emailArray = array();
            $dataApply = $this->input->post("check");
            foreach($dataApply as $dataId){
                $dataGetApply = $this->admin_models->getApplyByIdApplyJob($dataId)->row();
    
                $dataArrayEach = array(
                  'id_vacancies' => $dataGetApply->id_vacancies,
                  'id_user' => $dataGetApply->id_user,
                  'score' => $dataGetApply->score,
                  'lulus_test_online' => $dataGetApply->lulus_test_online,
                  'lulus_interview' => 'y',
                );
                
                
                $emailGetById = $this->admin_models->getEmailUserByIdUser($dataGetApply->id_user)->row();
                array_push($emailArray,$emailGetById->email);
                $this->admin_models->updateApplyJob($dataId, $dataArrayEach);
            }

            $this->load->library('phpmailer_lib');

              $mail = $this->phpmailer_lib->load();
      
              $mail->isSMTP();
              $mail->Host = 'smtp.gmail.com';
              $mail->SMTPAuth = true;
              $mail->Username = 'cs.recruitment123@gmail.com';
              $mail->Password = 'rekrut123';
              $mail->SMTPSecure = 'tls';
              $mail->Port = 587;
      
              $mail->setFrom('cs.recruitment123@gmail.com', 'Congratulations Recruitment');
              $mail->addReplyTo('cs.recruitment123@gmail.com', 'Congratulations Recruitment');
      
              // $mail->addAddress($emailUser);
              // $mail->AddAddress('person2@domain.com', 'Person Two');
              foreach($emailArray as $emailAddress){
                $mail->AddAddress(trim($emailAddress));
                }
      
              $mail->Subject = "Congratulations Recruitment";
      
              $mail->isHtml(true);
      
              $mailContent = "<html>
              <head>
                  <meta charset='utf-8'>
                  <meta http-equiv='X-UA-Compatible' content='IE=edge'>
                  <title>E-Recruitment</title>
                  <meta name='viewport content='width=device-width, initial-scale=1'>
              </head>
              
              <body style'background: #eaeaea; font-size: 15px; font-family:Arial;'>
              <table cellspacing='0'
                    style='margin: 0 auto; width: 600px; border: 1px solid #ccc; border-radius: 5px; background: #FFF'>
                  <tr>
                      <td style='padding: 15px 50px; width: 50%;'>
                          <h3>Congratulations Recruitment</h3>
                      </td>
                  </tr>
                  <tr>
                      <td colspan='2' style='font-family:auto; padding: 20px 50px;text-align: justify;line-height: 25px;'>
					  
                        Thank you for your application to PT Megah Mas Prima.
						Your qualifications have been thoroughly studied and evaluated and we believe you qualify well for this position.

						We hope you can come to our company 3 days after we send this email notification to take the next step to join our company.

						thank you,
						PT Megah Mas Prima
						
                      </td>
                  </tr>
                 
                
                  <tr style='background: #1eadff;'>
                      <td colspan='2'>
                          <div>
                              <div style='margin-top: 30px;'>
                                  <p style='font-family:Arial; text-align: center; color: #FFF'>Follow Us</p>
                              </div>
                          </div>
                      </td>
                  </tr>
                  <tr style='background: #1eadff;'>
                      <td colspan='2'>
                          <div style='text-align: center;padding:50px'>
                                  <span>
                                      <a href='https://www.facebook.com/pinopi.official' target='_blank'>
                                          <img src='http://pembalutsani.com/assets/user/images/v2/home/email/fb.png' width='40' />
                                      </a>
                                  </span>
                              <span style='margin: 0 20px;'>
                                      <a href='https://www.instagram.com/pinopi.official/'>
                                          <img src='http://pembalutsani.com/assets/user/images/v2/home/email/yt.png' width='40' />
                                      </a>
                                  </span>
                              <span>
                                      <a href='https://www.youtube.com/c/pinopi'>
                                          <img src='http://pembalutsani.com/assets/user/images/v2/home/email/ig.png' width='40' />
                                      </a>
                                  </span>
                          </div>
                      </td>
                  </tr>
                </table>
              </body>
              
              </html>";

      $mail->Body = $mailContent;
          
      if(!$mail->send()){
          echo 'Pesan Berhasil gagal dikirim';
          echo 'Pesan Error : ' . $mail->ErrorInfo;
      }else{
        $dataVacancy = $this->admin_models->getVacanciesById($idVacancy)->row();

        $dataArray = array(
          'unit' => $dataVacancy->unit,
          'position' => $dataVacancy->position,
          'sub_unit' => $dataVacancy->sub_unit,
          'deadline' => $dataVacancy->deadline,
          'location' => $dataVacancy->location,
          'quantity' => $dataVacancy->quantity,
          'type' => $dataVacancy->type,
          'salary' => $dataVacancy->salary,
          'level' => $dataVacancy->level,
          'education' => $dataVacancy->education,
          'responsibillities' => $dataVacancy->responsibillities,
          'requirements' => $dataVacancy->requirements,
          'experience' => $dataVacancy->experience,
          'status' => "done",
          'action' => "done",
          'createdBy' => $this->session->userdata('username'),
          );
  
           $hasil = $this->admin_models->updateJobVacancy($idVacancy, $dataArray);
           $this->session->set_flashdata('item', 'update');
           redirect(base_url('admin/listJob'));
      }
        }

        

      }else{
        $data['applicant'] = $this->admin_models->getApplicantGte($idVacancy);
        $data['vacancy'] = $this->admin_models->getVacanciesById($idVacancy);
        $this->load->view('admin/template/header');
        $this->load->view('admin/resultInterview', $data);
        $this->load->view('admin/template/footer');
      }
      
    }
    
    public function formVacancies()
    {
      if(!$this->session->userdata('username')){
        redirect('admin/login');
      }

      if(isset($_POST['submit'])){

        $data = array(
          'unit' => $this->input->post('unit'),
          'position' => $this->input->post('position'),
          'sub_unit' => $this->input->post('sub_unit'),
          'deadline' => $this->input->post('deadline'),
          'location' => $this->input->post('location'),
          'quantity' => $this->input->post('quantity'),
          'type' => $this->input->post('type'),
          'salary' => $this->input->post('salary'),
          'level' => $this->input->post('level'),
          'education' => $this->input->post('education'),
          'responsibillities' => $this->input->post('responsibillities'),
          'requirements' => $this->input->post('requirements'),
          'experience' => $this->input->post('experience'),
          'unit' => $this->input->post('unit'),
          'status' => "waiting_for_applicant",
          'action' => "input_interview_schedule",
          'createdBy' => $this->session->userdata('username')
        );

        $this->admin_models->insertVacancies($data);
        $this->session->set_flashdata('item', 'add');
          redirect(base_url('admin/listJob'));
      }else{

      $this->load->view('admin/template/header');
      $this->load->view('admin/form/form_vacancies');
      $this->load->view('admin/template/footer');
      }
    }

    public function formEditQuestion($idSoal)
    {
      if(!$this->session->userdata('username')){
        redirect('admin/login');
      }

      if(isset($_POST['submit'])){
        
        $dataSoalJawaban = array(
            'soal' => $this->input->post('soal'),
            'jawaban' => $this->input->post('jawaban'),
            );
        
        $dataPilihan = array(
                'pilihan_a' => $this->input->post('pil_a'),
                'pilihan_b' => $this->input->post('pil_b'),
                'pilihan_c' => $this->input->post('pil_c'),
                'pilihan_d' => $this->input->post('pil_d'),
                'pilihan_e' => $this->input->post('pil_e'),
                );

             $hasil = $this->admin_models->updateSoalJawaban($idSoal, $dataSoalJawaban);
             $hasil = $this->admin_models->updatePilihanJawaban($idSoal, $dataPilihan);
        
            $this->session->set_flashdata('item', 'update');
            redirect(base_url('admin/testOnline'));

        }else{
        $data['test'] = $this->user_models->getTestGeneral($idSoal)->row();
        $this->load->view('admin/template/header');
        $this->load->view('admin/form/form_edit_question', $data);
        $this->load->view('admin/template/footer');
      }
    }

	public function login()
	{
        if(isset($_POST['submit'])){
            $username = $this->input->post('username');
            $password	 = $this->input->post('password');
            $berhasil = $this->admin_models->login($username, $password);
            // echo $berhasil;

            if ($berhasil == 1) {
                $this->session->set_userdata(array('username' => $username ));
                redirect(base_url('admin'));
            }else{
        
      $this->session->set_flashdata('item', 'gagal');
                    redirect(base_url('admin/login'));
            }

        }else{
            $this->load->view('admin/form_login');
        }
    }
    
    public function logout(){
		$this->session->sess_destroy();
			redirect(base_url('user/home'));
    }
    
}
