<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class User extends CI_Controller {
    public function home(){
      $this->load->view('user/template/headerHome');
      $this->load->view('user/template/footer');
    }

    public function jobVacancy(){
      if(!$this->session->userdata('username')){
        redirect('user/login');
      }
      $idUser = $this->session->userdata('idUser');
      $check = $this->user_models->getCheckForm($idUser)->row();
      // || (empty($check->institutionName))
      if( (empty($check->fullName))  || (empty($check->educationLevel))){
        $this->session->set_flashdata('item', 'empty');
        redirect(base_url('user'));
      }else{
        $data['vacancies'] = $this->admin_models->getVacancies();
      
        if(!$this->session->userdata('username')){
          $this->load->view('user/template/headerNoBanner');
        }else{
          $this->load->view('user/template/header');
        }
        
        $this->load->view('user/pages/job_vacancy', $data);
        $this->load->view('user/template/footer');
      }

    }

    public function testOnlineGeneral($id){
      if(!$this->session->userdata('username')){
        redirect('user/login');
      }
      
      $totalPage = $this->user_models->getAllTestGeneral()->num_rows();
      $nextPage = intval($id+1);
      
      if(isset($_POST['submit'])){
      if(!empty($this->input->post("jawaban"))){

        if( $this->input->post('jawaban') === $this->input->post('jawabanTrue')){
          $result = "y";
        }else{
          $result = "t";
        }
        $data = array(
          'id_user' => $this->session->userdata('idUser'),
          'id_soal' => $this->input->post('id_soal'),
          'jawaban' => $this->input->post('jawaban'),
          'hasil' => $result,
        );

        $this->user_models->insertTestOnline($data);
        if($id < $totalPage){
         redirect("user/testOnlineGeneral/$nextPage");
        }else{
         redirect("user/resultTestOnlineGeneral");
        }
      }else{
        $this->session->set_flashdata('item', 'empty');
        redirect("user/testOnlineGeneral/$id");
      }
      }else{
      
      $data['test'] = $this->user_models->getTestGeneral($id);
      $data['sum_test'] = $this->user_models->getAllTestGeneral();

      $this->load->view('user/template/header');
      $this->load->view('user/pages/test_online_general', $data);
      $this->load->view('user/template/footer');
      }
      
    }

    public function resultTestOnlineGeneral(){
      if(!$this->session->userdata('username')){
        redirect('user/login');
      }
      
      $idUser = $this->session->userdata('idUser');
      $scoreCount = $this->user_models->resultTestOnline($idUser)->result();

      $applyData = $this->user_models->getApplyByIdUser($idUser)->row();

     
      $scoreFinal = 0;
      foreach($scoreCount as $data) {
        if($data->hasil === "y"){
          $scoreFinal += 5;
        }
      }

      $hasilTestOnline = 't';
      if($scoreFinal > 50){
        $hasilTestOnline = 'y';
      }

      $data = ['score' => $scoreFinal];
      
      $dataArray = array(
        'id_apply_job' => $applyData->id_apply_job,
        'id_vacancies' => $applyData->id_vacancies,
        'score' => $scoreFinal,
        'lulus_test_online' => $hasilTestOnline,
        'lulus_interview' => 't'
        );

        $hasil = $this->user_models->updateApplyJob($applyData->id_apply_job, $dataArray);


      $this->load->view('user/template/header');
      $this->load->view('user/pages/result_test_online_general', $data);
      $this->load->view('user/template/footer');
    }

    public function resultApplyJob($idJob){
      if(!$this->session->userdata('username')){
        redirect('user/login');
      }
      
      $idUser = $this->session->userdata('idUser');
      // $scoreCount = $this->user_models->resultTestOnline($idUser)->result();
      
      // $applyData = $this->user_models->getApplyByIdJob($idJob)->row();

      // $scoreFinal = 0;
      // foreach($scoreCount as $data) {
      //   if($data->hasil === "y"){
      //     $scoreFinal += 5;
      //   }
      // }

      // $data = ['score' => $scoreFinal];
      
      // $dataArray = array(
       
      //   'id_vacancies' => $applyData->id_vacancies,
      //   'score' => $scoreFinal,
      //   'id_apply_job' => $applyData->id_apply_job,
      //   );
      
      //   var_dump($dataArray);
      //   $hasil = $this->user_models->updateApplyJob($applyData->id_apply_job, $dataArray);

      $this->load->view('user/template/header');
      $this->load->view('user/pages/result_apply_job');
      $this->load->view('user/template/footer');
    }

    public function applyJob($idJob){
      $idUser = $this->session->userdata('idUser');
      
      $isApply = $this->admin_models->getApplyByIdJobAndUser($idJob,$idUser);

      var_dump(empty($isApply->result()));

      if(empty($isApply->result())){
        $check = $this->user_models->getIsTest($idUser);
        if(empty($check->result())){

          $data = array(
            'id_user' => $this->session->userdata('idUser'),
            'id_vacancies' => $idJob,
            'lulus_test_online' => 't',
            'lulus_interview' => 't'
            );
    
            $this->user_models->insertApplyJob($data);

          redirect("user/testOnlineGeneral/1");
        }else{

          $scoreCount = $this->user_models->resultTestOnline($idUser)->result();

          $scoreFinal = 0;
          foreach($scoreCount as $data) {
            if($data->hasil === "y"){
              $scoreFinal += 5;
            }
          }
    
          $data = ['score' => $scoreFinal];

          $hasilTestOnline = 't';
          if($scoreFinal > 50){
            $hasilTestOnline = 'y';
          }

          $data = array(
            'id_user' => $this->session->userdata('idUser'),
            'score' => $scoreFinal,
            'id_vacancies' => $idJob,
            'lulus_test_online' => $hasilTestOnline,
            'lulus_interview' => 't'
            );
    
            $this->user_models->insertApplyJob($data);

          $this->session->set_flashdata('item', 'apply');
          redirect("user/resultApplyJob/$idJob");
        }
        
      }else{
        $this->session->set_flashdata('item', 'isApply');
        redirect("user/detailJobVacancy/$idJob");
      }

     
    }

    public function detailJobVacancy($id){
      if(!$this->session->userdata('username')){
        redirect('user/login');
      }

      $idUser = $this->session->userdata('idUser');
      $data['vacancy'] = $this->admin_models->getVacanciesById($id);
      $data['check'] = $this->user_models->getIsTest($idUser);

      $this->load->view('user/template/header');
      $this->load->view('user/pages/detail_job_vacancy', $data);
      $this->load->view('user/template/footer');
    }

    public function index()
	{
    if(!$this->session->userdata('username')){
      redirect('user/login');
    }
    $idUser = $this->session->userdata('idUser');
    $data['data_personal'] = $this->user_models->getPersonalData($idUser);
    $data['formal_education'] = $this->user_models->getFormalEducation($idUser);
    $data['non_formal_education'] = $this->user_models->getNonFormalEducation($idUser);
    $data['family'] = $this->user_models->getFamily($idUser);
    $data['work_experience'] = $this->user_models->getWorkingExpereince($idUser);
    $data['motivation'] = $this->user_models->getMotivation($idUser);

    $this->load->view('user/template/header');
    $this->load->view('user/pages/menus');
    $this->load->view('user/pages/personal_data', $data);
    $this->load->view('user/pages/formal_education', $data);
    $this->load->view('user/pages/non_formal_education', $data);
    $this->load->view('user/pages/family', $data);
    $this->load->view('user/pages/work_experience', $data); 
    $this->load->view('user/pages/motivation', $data); 
    $this->load->view('user/template/footer');
    }
    
    public function login()
	{
        if(isset($_POST['submit'])){
            $email = $this->input->post('email');
            $password	 = $this->input->post('password');
            $berhasil = $this->user_models->login($email, $password);
            if ($berhasil->num_rows() == 1) {

              $check = $this->user_models->checkVerify($email, $password);
              if($check->num_rows() == 1){
                $this->session->set_userdata(array('username' => $berhasil->row()->username, 'idUser' =>$berhasil->row()->id_user));
                redirect(base_url('user'));
              }else{
                $this->session->set_flashdata('item', 'unverified');
                    redirect(base_url('user/login'));          
              }
            }else{
        
      $this->session->set_flashdata('item', 'gagal');
                    redirect(base_url('user/login'));
            }

        }else{
            $this->load->view('user/form_login');
        }
    }

    public function forgotPassword()
    {
          if(isset($_POST['submit'])){
            $email = $this->input->post('email');
            $this->session->set_flashdata('item', 'empty');
            redirect(base_url("user/changePassword/".urlencode($email)));   

          }else{
              $this->load->view('user/form_forgot_password');
          }
      }

      public function changePassword($email)
      {
            if(isset($_POST['submit'])){

              $emailAccount = urldecode($email);
              $newPassword = $this->input->post('passwordNew');
              $confirmPassword = $this->input->post('passwordConfirm');
              // var_dump($this->input->post());
              // var_dump($emailAccount);
              
              if($newPassword === $confirmPassword){
                $data = $this->user_models->isRegister($emailAccount);
                var_dump($data->row());
                if(!empty($data->row())){
                  $dataArray = array(
                    'username' => $data->row()->username,
                    'email' => $data->row()->email,
                    'password' => md5($confirmPassword),
                    'status'  => $data->row()->status
                  );

                  $update = $this->user_models->updatePassword($data->row()->id_user, $dataArray);
                  var_dump($update);

                  if($update === 1){
                    $this->session->set_flashdata('item', 'update');
                  redirect(base_url("user/login")); 
                  }
                }else{
                  $this->session->set_flashdata('item', 'gagal');
                  redirect(base_url("user/login"));  
                }

              }else{
                $this->session->set_flashdata('item', 'unmatch');
                redirect(base_url("user/changePassword/$email"));  
              }
            }else{
                $this->load->view('user/form_change_password');
            }
        }

	public function register()
	{
        if(isset($_POST['submit'])){
          $emailUser = $this->input->post('email');
          $check = $this->user_models->isRegister($emailUser);

          if(empty($check->row())){
            
            $data = array(
                'username' => $this->input->post('username'),
                'email' => $this->input->post('email'),
                'password' => md5($this->input->post('password')),
                'status'  => "unverified"
              );

            $berhasil = $this->user_models->register($data);
            
            $data = $this->user_models->isRegister($emailUser);
            $idUser = $data->row()->id_user; 
            if ($berhasil) {

              $this->load->library('phpmailer_lib');

              $mail = $this->phpmailer_lib->load();
      
              $mail->isSMTP();
              $mail->Host = 'smtp.gmail.com';
              $mail->SMTPAuth = true;
              $mail->Username = 'cs.recruitment123@gmail.com';
              $mail->Password = 'rekrut123';
              $mail->SMTPSecure = 'tls';
              $mail->Port = 587;
      
              $mail->setFrom('cs.recruitment123@gmail.com', 'Account Verification');
              $mail->addReplyTo('cs.recruitment123@gmail.com', 'Account Verification');
      
              $mail->addAddress($emailUser);
      
              $mail->Subject = "Account Verification";
      
              $mail->isHtml(true);
      
              $mailContent = "
              <html>
              <body style='display: flex; justify-content: center;'>
              <table style='width: 500px; border: 1px solid #000; margin-top: 50px;'>
                  <tr>
                      <th>
                          <h2 style='margin-top: 20px;'>Welcome to E-Recruitment</h2>
                      </th>
                  </tr>
                  <tr>
                      <td style='padding-left: 10px;'>
                          <h2>Salam</h2>
                      </td>
                  </tr>
                  <tr>
                      <td style='padding: 10px;'>
                         Silahkan klik tombol dibawah ini untuk memverifikasi akun e-Recruitment anda! 
                      </td>
                  </tr>
                     <tr>
                     
                  </tr>
                  <tr>
                      <td style='text-align: center; padding: 0 10px;'>
                          <a href=`http://localhost/e-recruitment/user/verifyAccount/{$idUser}`>
                              <button style='width: 100%; height: 40px; background: #1eadff; cursor: pointer;'>
                                  <span style='color: #FFF; font-size: 15px;'>Verifikasi Sekarang</span>
                              </button>
                          </a>
                      </td>
                  </tr>
                  <tr>
                      <td style='padding: 10px;'>
                          Salam hangat, Tim Recruitment
                      </td>
                  </tr>
              </table>
              </body>
              </html>";
      
              $mail->Body = $mailContent;
      
              if(!$mail->send()){
                  echo 'Pesan Berhasil gagal dikirim';
                  echo 'Pesan Error : ' . $mail->ErrorInfo;
              }else{
                  echo 'Pesan Berhasil dikirim';
              }

                $this->session->set_flashdata('item', 'berhasil');
                redirect(base_url('user/register'));
            }else{
                $this->session->set_flashdata('item', 'gagal');
                redirect(base_url('user/register'));
            }
          }else{
            $this->session->set_flashdata('item', 'terdaftar');
            redirect(base_url('user/register'));
          }
        }else{
            $this->load->view('user/form_register');
        }
    }
    
    public function verifyAccount($idUser){

      $data = array(
        'status' => "verified");

      $hasil = $this->user_models->verifiedAccount($idUser, $data);
      if($hasil){
        $this->session->set_flashdata('item', 'verified');
      $this->load->view('user/verifikasi_account');
      }else{
        $this->session->set_flashdata('item', 'gagal');
          $this->load->view('user/verifikasi_account');
      }

      // $this->load->view('user/verifikasi_account');
    }

    public function formPersonalData(){
        if(isset($_POST['submit'])){

            $config['upload_path']          = './assets/images/profile';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;
      
            $this->load->library('upload', $config);
      
            if ( !$this->upload->do_upload('img')){
              $error = array('error' => $this->upload->display_errors());
              $dump = 'ERROR';
              var_dump($error);
            }else{
                $destination = 'assets/images/profile/';
                $fileName = $this->upload->data('file_name');
                $data = array(
                    'id_user' => $this->session->userdata('idUser'),
                    'fullName' => $this->input->post('fullName'),
                    'placeBirth' => $this->input->post('placeBirth'),
                    'dateBirth' => $this->input->post('dateBirth'),
                    'gender'  => $this->input->post('gender'),
                    'height'  => $this->input->post('height'),
                    'weight'  => $this->input->post('weight'),
                    'religion'  => $this->input->post('religion'),
                    'phoneNumber'  => $this->input->post('phoneNumber'),
                    'address'  => $this->input->post('address'),
                    'gambarProfil' =>  $destination.$fileName
                  );
                  
            $this->user_models->insertPersonalData($data);
            $this->session->set_flashdata('item', 'update');
            redirect(base_url('user'));
          }
            }else{
                $this->load->view('user/template/headerNoBanner');
                $this->load->view('user/form/personal_data');
                $this->load->view('user/template/footer');
            }
    }

    public function formFormalEducation(){
        if(isset($_POST['submit'])){
            var_dump($this->input->post());

            $config['upload_path']          = './assets/images/certificate';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 10000;
            $config['max_width']            = 10000;
            $config['max_height']           = 10000;
      
            $this->load->library('upload', $config);
      
            if ( !$this->upload->do_upload('img')){
              $error = array('error' => $this->upload->display_errors());
              $dump = 'ERROR';
              var_dump($error);
            }else{
                $destination = 'assets/images/certificate/';
                $fileName = $this->upload->data('file_name');
                $data = array(
                    'id_user' => $this->session->userdata('idUser'),
                    'educationLevel' => $this->input->post('educationLevel'),
                    'educationInstitution' => $this->input->post('educationInstitution'),
                    'certificateSeries' => $this->input->post('certificateSeries'),
                    'yearEntry'  => $this->input->post('yearEntry'),
                    'graduationYear'  => $this->input->post('graduationYear'),
                    'institutionPlace'  => $this->input->post('institutionPlace'),
                    'certificateNumber'  => $this->input->post('certificateNumber'),
                    'imgCertificate'  => $destination.$fileName
                  );
                  
            $this->user_models->insertFormalEducation($data);
            $this->session->set_flashdata('item', 'update');
            redirect(base_url('user'));
          }

        }else{
        $this->load->view('user/template/headerNoBanner');
        $this->load->view('user/form/formal_education');
        $this->load->view('user/template/footer');
        }
    }

    public function formNonFormalEducation(){
      if(isset($_POST['submit'])){
          var_dump($this->input->post());

          $config['upload_path']          = './assets/images/certificate';
          $config['allowed_types']        = 'gif|jpg|png|jpeg';
          $config['max_size']             = 10000;
          $config['max_width']            = 10000;
          $config['max_height']           = 10000;
    
          $this->load->library('upload', $config);
    
          if ( !$this->upload->do_upload('img')){
            $error = array('error' => $this->upload->display_errors());
            $dump = 'ERROR';
            var_dump($error);
          }else{
              $destination = 'assets/images/certificate/';
              $fileName = $this->upload->data('file_name');
              $data = array(
                  'id_user' => $this->session->userdata('idUser'),
                  'trainingName' => $this->input->post('trainingName'),
                  'trainingScope' => $this->input->post('trainingScope'),
                  'trainingInstitution' => $this->input->post('trainingInstitution'),
                  'certificateNumber'  => $this->input->post('certificateNumber'),
                  'startDate'  => $this->input->post('startDate'),
                  'endDate'  => $this->input->post('endDate'),
                  'imgCertificate'  => $destination.$fileName
                );
                
          $this->user_models->insertNonFormalEducation($data);
          $this->session->set_flashdata('item', 'update');
          redirect(base_url('user'));
        }

      }else{
      $this->load->view('user/template/headerNoBanner');
      $this->load->view('user/form/non_formal_education');
      $this->load->view('user/template/footer');
      }
  }

    public function formFamily(){
      if(isset($_POST['submit'])){
          $data = array(
            'id_user' => $this->session->userdata('idUser'),
            'name' => $this->input->post('name'),
            'placeBirth' => $this->input->post('placeBirth'),
            'dateBirth' => $this->input->post('dateBirth'),
            'status' => $this->input->post('status'),
            'lastEducation'  => $this->input->post('lastEducation'),
            'profession'  => $this->input->post('profession'),
          );

          $this->user_models->insertFamily($data);
          $this->session->set_flashdata('item', 'update');
            redirect(base_url('user'));

        }else{
          $this->load->view('user/template/headerNoBanner');
          $this->load->view('user/form/family');
          $this->load->view('user/template/footer');
        }
    }

    public function formWorkExperience(){
      if(isset($_POST['submit'])){
        $data = array(
          'id_user' => $this->session->userdata('idUser'),
          'institutionName' => $this->input->post('institutionName'),
          'position' => $this->input->post('position'),
          'startDate' => $this->input->post('startDate'),
          'endDate' => $this->input->post('endDate'),
          'jobDescription'  => $this->input->post('jobDescription'),
          'reasonResign'  => $this->input->post('reasonResign'),
        );

        $this->user_models->insertWorkExperience($data);
        $this->session->set_flashdata('item', 'update');
          redirect(base_url('user'));
        }else{
          $this->load->view('user/template/headerNoBanner');
          $this->load->view('user/form/work_experience');
          $this->load->view('user/template/footer');
        }
        
    }

    public function formMotivation(){
      if(isset($_POST['submit'])){

        $data = array(
          'id_user' => $this->session->userdata('idUser'),
          'reasonChoose' => $this->input->post('reasonChoose'),
          'expertise' => $this->input->post('expertise'),
          'expectedSallary' => $this->input->post('expectedSallary'),
          'hobby' => $this->input->post('hobby'),
          'motivation'  => $this->input->post('motivation'),
        );

        $this->user_models->insertMotivation($data);
        $this->session->set_flashdata('item', 'update');
        redirect(base_url('user'));

        }else{
          $this->load->view('user/template/headerNoBanner');
          $this->load->view('user/form/motivation');
          $this->load->view('user/template/footer');
        }
    }

    public function deletePersonalData(){
      $idUser = $this->session->userdata('idUser');
      $table = 'tbl_personal_data';

      $this->user_models->deleteQuery($idUser, $table);
      $this->session->set_flashdata('item', 'delete');
      redirect(base_url('user'));
    }

    public function deleteFormalEducation(){
      $idUser = $this->session->userdata('idUser');
      $table = 'tbl_formal_education';

      $this->user_models->deleteQuery($idUser, $table);
      $this->session->set_flashdata('item', 'delete');
      redirect(base_url('user'));
    }

    public function deleteFamily(){
      $idUser = $this->session->userdata('idUser');
      $table = 'tbl_family';

      $this->user_models->deleteQuery($idUser, $table);
      $this->session->set_flashdata('item', 'delete');
      redirect(base_url('user'));
    }

    public function deleteMotivation(){
      $idUser = $this->session->userdata('idUser');
      $table = 'tbl_motivation';

      $this->user_models->deleteQuery($idUser, $table);
      $this->session->set_flashdata('item', 'delete');
      redirect(base_url('user'));
    }

    public function deleteWorkExperience(){
      $idUser = $this->session->userdata('idUser');
      $table = 'tbl_work_experience';

      $this->user_models->deleteQuery($idUser, $table);
      $this->session->set_flashdata('item', 'delete');
      redirect(base_url('user'));
    }

    public function logout(){
		$this->session->sess_destroy();
			redirect(base_url('user/home'));
    }
    
}
