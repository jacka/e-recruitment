<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_models extends CI_Model {

	public function login($email, $password)
	{

    $periksa = $this->db->get_where('tbl_users',  array('email' =>$email, 'password' => md5($password) ));
	return $periksa;	
	}

	public function isRegister($email)
	{

    $periksa = $this->db->get_where('tbl_users',  array('email' =>$email));
	return $periksa;	
	}

	function updatePassword($idusers, $data){
		$this->db->where('id_user',$idusers);
		$result = $this->db->update('tbl_users',$data);
		return $this->db->affected_rows();
	}

	public function checkVerify($email, $password)
	{

    $periksa = $this->db->get_where('tbl_users',  array('email' =>$email, 'password' => md5($password), 'status'=> 'verified' ));
	return $periksa;	
	}

	
	function getPersonalData($id){
		$this->db->where('id_user', $id);
		$hasil = $this->db->get('tbl_personal_data');
		return $hasil;
	}

	function getFormalEducation($id){
		$this->db->where('id_user', $id);
		$hasil = $this->db->get('tbl_formal_education');
		return $hasil;
	}

	function getNonFormalEducation($id){
		$this->db->where('id_user', $id);
		$hasil = $this->db->get('tbl_non_formal_education');
		return $hasil;
	}

	function getFamily($id){
		$this->db->where('id_user', $id);
		$hasil = $this->db->get('tbl_family');
		return $hasil;
	}

	function getWorkingExpereince($id){
		$this->db->where('id_user', $id);
		$hasil = $this->db->get('tbl_work_experience');
		return $hasil;
	}

	function getMotivation($id){
		$this->db->where('id_user', $id);
		$hasil = $this->db->get('tbl_motivation');
		return $hasil;
	}

	function getIsTest($id){
		$this->db->where('id_user', $id);
		$hasil = $this->db->get('tbl_hasil_jawaban');
		return $hasil;
	}

	function updateApplyJob($idusers, $data){
		$this->db->where('id_apply_job',$idusers);
		$result = $this->db->update('tbl_apply_job',$data);
		return $result;
		}

	function getApplyByIdUser($id){
		$this->db->where('id_user', $id);
		$hasil = $this->db->get('tbl_apply_job');
		return $hasil;
	}

	function getApplyByIdJob($id){
		$this->db->where('id_vacancies', $id);
		$hasil = $this->db->get('tbl_apply_job');
		return $hasil;
	}

	function getIsApply($id){
		$this->db->where('id_vacancies', $id);
		$hasil = $this->db->get('tbl_apply_job');
		return $hasil;
	}

	function resultTestOnline($id){
		$query = $this->db->query("SELECT hasil FROM tbl_hasil_jawaban WHERE id_user=$id");
		return $query;
	}


    public function register($data)
	{
        $insert = $this->db->insert("tbl_users", $data);
        if($this->db->affected_rows() == 1){
            return true;
        }else{
            return false;
        }
	}

	public function insertApplyJob($data){
		$this->db->insert('tbl_apply_job',$data);
	}

	public function insertPersonalData($data){
		$this->db->insert('tbl_personal_data',$data);
	}

	public function insertFormalEducation($data){
		$this->db->insert('tbl_formal_education',$data);
	}

	public function insertNonFormalEducation($data){
		$this->db->insert('tbl_non_formal_education',$data);
	}

	public function insertFamily($data){
		$this->db->insert('tbl_family',$data);
	}

	public function insertWorkExperience($data){
		$this->db->insert('tbl_work_experience',$data);
	}

	public function insertMotivation($data){
		$this->db->insert('tbl_motivation',$data);
	}

	public function insertTestOnline($data){
		$this->db->insert('tbl_hasil_jawaban',$data);
	}

	function verifiedAccount($idusers, $data){
		$this->db->where('id_user',$idusers);
		$result = $this->db->update('tbl_users',$data);
		return $result;
		}

	function deleteQuery($idusers, $table){
		$this->db->where('id_user',$idusers);
		$this->db->delete($table);
	}

	function getTestGeneral($id){
	$query = $this->db->query("SELECT * FROM tbl_soal a LEFT JOIN tbl_pilihan_jawaban b ON a.id_soal = b.id_soal WHERE a.id_soal=$id");
		return $query;
	}

	function getTestGeneralWithoutId(){
		$query = $this->db->query("SELECT * FROM tbl_soal a LEFT JOIN tbl_pilihan_jawaban b ON a.id_soal = b.id_soal");
			return $query;
		}

	function getAllTestGeneral(){
		$hasil = $this->db->get('tbl_soal');
		return $hasil;
	}

	function getCheckForm($id){
		$query = $this->db->query("SELECT b.fullName, c.institutionName, d.educationLevel FROM tbl_users a LEFT JOIN tbl_personal_data b ON b.id_user=a.id_user LEFT JOIN tbl_work_experience c ON c.id_user = a.id_user LEFT JOIN tbl_formal_education d ON d.id_user = a.id_user WHERE a.id_user = $id GROUP BY a.id_user");
			return $query;
		}

	

}
