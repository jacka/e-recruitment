<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_models extends CI_Model {

	public function login($username, $password)
	{

    $periksa = $this->db->get_where('tbl_admin',  array('username' =>$username, 'password' => md5($password) ));
		if($periksa->num_rows() > 0){
			return 1;
		}else{
			// return $this->db->get_compiled_select();
			return 0;
		}
	}

	public function insertVacancies($data){
		$this->db->insert('tbl_vacancies',$data);
	}

	function getVacancies(){
		$hasil = $this->db->get('tbl_vacancies');
		return $hasil;
	}

	function getApplyJob(){
		$hasil = $this->db->get('tbl_apply_job');
		return $hasil;
	}

	function getVacanciesById($id){
		$this->db->where('id_vacancies', $id);
		$hasil = $this->db->get('tbl_vacancies');
		return $hasil;
	}

	function getApplyById($id){
		$this->db->where('id_user', $id);
		$hasil = $this->db->get('tbl_apply_job');
		return $hasil;
	}

	function getApplyByIdApplyJob($id){
		$this->db->where('id_apply_job', $id);
		$hasil = $this->db->get('tbl_apply_job');
		return $hasil;
	}

	function getTotalApplicant(){
		$query = $this->db->query("SELECT * FROM `tbl_apply_job` GROUP BY id_user");
		return $query;
	}

	function getLulusTestOnline(){
		$query = $this->db->query("SELECT * FROM tbl_apply_job WHERE lulus_test_online='y'");
			return $query;
		}

	function getLulusTestInterview(){
			$query = $this->db->query("SELECT * FROM tbl_apply_job WHERE lulus_interview='y'");
				return $query;
			}
	
	function getEmailUserByIdUser($id){
			$query = $this->db->query("SELECT email FROM tbl_users WHERE id_user=$id");
				return $query;
	}

			
	function getApplyByIdJobAndUser($idJob, $idUser){
		$query = $this->db->query("SELECT * FROM tbl_apply_job WHERE id_vacancies=$idJob AND id_user=$idUser");
		return $query;
	}

	function getApplicant($id){
		$query = $this->db->query("SELECT a.id_vacancies, a.id_user, b.*, d.educationInstitution, d.educationLevel, c.institutionName, a.score, e.email, f.position FROM tbl_apply_job a LEFT JOIN tbl_personal_data b ON b.id_user = a.id_user LEFT JOIN tbl_work_experience c ON c.id_user = a.id_user LEFT JOIN tbl_formal_education d ON d.id_user = a.id_user LEFT JOIN tbl_users e ON e.id_user = a.id_user LEFT JOIN tbl_vacancies f ON f.id_vacancies = a.id_vacancies WHERE a.id_vacancies = $id");
			return $query;
		}

	function getApplicantGte($id){
			$query = $this->db->query("SELECT a.id_apply_job, a.id_vacancies, a.id_user, b.*, d.educationInstitution, d.educationLevel, c.institutionName, a.score, e.email, f.position FROM tbl_apply_job a LEFT JOIN tbl_personal_data b ON b.id_user = a.id_user LEFT JOIN tbl_work_experience c ON c.id_user = a.id_user LEFT JOIN tbl_formal_education d ON d.id_user = a.id_user LEFT JOIN tbl_users e ON e.id_user = a.id_user LEFT JOIN tbl_vacancies f ON f.id_vacancies = a.id_vacancies WHERE a.id_vacancies = $id AND a.score > 50");
				return $query;
			}
	
	function updateApplyJob($id, $data){
				$this->db->where('id_apply_job',$id);
				$result = $this->db->update('tbl_apply_job',$data);
				return $result;
		}

	function updateJobVacancy($idVacancy, $data){
			$this->db->where('id_vacancies',$idVacancy);
			$result = $this->db->update('tbl_vacancies',$data);
			return $result;
	}
	
	function getAllApplicant(){
		$query = $this->db->query("SELECT a.id_user, b.fullName, b.gender, b.dateBirth, d.educationLevel, d.educationInstitution, c.institutionName, a.score, e.email, g.position FROM tbl_apply_job a LEFT JOIN tbl_personal_data b ON b.id_user = a.id_user LEFT JOIN tbl_work_experience c ON c.id_user = a.id_user LEFT JOIN tbl_formal_education d ON d.id_user = a.id_user LEFT JOIN tbl_users e ON e.id_user = a.id_user LEFT JOIN tbl_apply_job f ON e.id_user = a.id_user LEFT JOIN tbl_vacancies g ON g.id_vacancies = f.id_vacancies GROUP BY a.id_user");
			return $query;
		}
		
	function getAllJobApplyByUser($idUser){
		$query = $this->db->query("SELECT b.position FROM tbl_apply_job a LEFT JOIN tbl_vacancies b ON b.id_vacancies = a.id_vacancies WHERE a.id_user=$idUser");
				return $query;
		}
	
	function getTestOnline(){
			$hasil = $this->db->get('tbl_soal');
			return $hasil;
		}	
		
	function updateSoalJawaban($idSoal, $data){
			$this->db->where('id_soal',$idSoal);
			$result = $this->db->update('tbl_soal',$data);
			return $result;
		}

	function updatePilihanJawaban($idSoal, $data){
			$this->db->where('id_soal',$idSoal);
			$result = $this->db->update('tbl_pilihan_jawaban',$data);
			return $result;
		}
}
