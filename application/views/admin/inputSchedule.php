

<?php
$data = $vacancy->row();

// var_dump($applicant->result());
?>
     <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
              <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>
              </span>
              Input Interview
            </h3>
            <div class="flash-data" data-vacancy="<?php echo $this->session->flashdata('item'); ?>"></div>

            <nav aria-label="breadcrumb">
              <ul class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">
                  <a class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" href="<?=base_url("admin/formVacancies")?>">Add Vacancy</a>
                <!-- <a type="button" class="btn btn-gradient-primary btn-rounded btn-fw" href="">Add Vacancy</a> -->
                  <!-- <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i> -->
                </li>
              </ul>
            </nav>
          </div>

          <div>

          <?php
          echo form_open("admin/inputSchedule/$data->id_vacancies");
          ?>

          <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Detail Vacancy</h4>
                  <table class="table">
                      <tr>
                        <td>
                          1
                        </td>
                        <td>
                          Vacancy Id
                        </td>
                        <td>
                        <b> : </b>
                        </td>
                        <td>
                        <?=$data->id_vacancies?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          2
                        </td>
                        <td>
                        Unit
                        </td>
                        <td>
                        <b> : </b>
                        </td>
                        <td>
                        <?=$data->unit?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          3
                        </td>
                        <td>
                        Position
                        </td>
                        <td>
                        <b> : </b>
                        </td>
                        <td>
                        <?=$data->position?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          4
                        </td>
                        <td>
                        Location
                        </td>
                        <td>
                        <b> : </b>
                        </td>
                        <td>
                        <?=$data->location?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          5
                        </td>
                        <td>
                        Type
                        </td>
                        <td>
                          <b> : </b>
                        </td>
                        <td>
                        <?=$data->type?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          6
                        </td>
                        <td>
                        Quantity
                        </td>
                        <td>
                          <b> : </b>
                        </td>
                        <td>
                        <?=$data->quantity?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          7
                        </td>
                        <td>
                        Interview Date
                        </td>
                        <td>
                          <b> : </b>
                        </td>
                        <td>
                        <input type="date" class="form-control" placeholder="dd/mm/yyyy" name="schedule" required/>
                        </td>
                      </tr>
                  </table>
                </div>
              </div>
            </div>

          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-body">
            <table id="table_id" class="display">
              <thead>
                  <tr>
                      <th>No</th>
                      <th>Name</th>
                      <th>Gender</th>
                      <th>Date Of Birth</th>
                      <th>Education</th>
                      <th>Experience</th>
                      <th>Test Score</th>
                      <th>Hasil Tes Online</th>
                      <th style="display:none">Email</th>
                  </tr>
              </thead>
              <tbody>
              <?php if(!empty($applicant->result())){
                $no=1;
                foreach($applicant->result() as $data) {
                ?>
                  <tr>
                      <td><?=$no;?></td>
                      <td><?=$data->fullName;?></td>
                      <td><?=$data->gender;?></td>
                      <td><?=$data->dateBirth;?></td>
                      <td><?=$data->educationLevel;?></td>
                      <?php if(!empty($data->institutionName)){ ?>
                      <td>True</td>
                      <?php }else { ?>
                        <td>False</td>
                      <?php } ?>
                      <td><?=$data->score;?></td>
                      <?php if($data->score > 50){ ?>
                        <td>Lulus</td>
                        <td style="display:none">  <input type="text" name="email[]" value=<?=$data->email?> > </td>
                      <?php }else { ?>
                        <td>Tidak Lulus</td>
                      <?php } ?>
                      
                      <!-- style="display:none" -->
                  </tr>
              <?php  $no++; } } else { ?>
                  <tr>
                  <td colspan="8" align="center">Data Is Empty</td>
                  </tr>
                <?php } ?>

          </table>
        </div>
        </div>


            </div>

          </div>

          <div style="display:flex;justify-content: flex-end;margin-top:20px">
            <button type="submit" class="btn btn-gradient-primary btn-fw" name="submit">Send Notification</button>
          </div>
        </form>
        </div>


        </div>
