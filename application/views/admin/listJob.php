
     <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
              <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>
              </span>
              To Do List
            </h3>
            <div class="flash-data" data-vacancy="<?php echo $this->session->flashdata('item'); ?>"></div>

            <nav aria-label="breadcrumb">
              <ul class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">
                  <a class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" href="<?=base_url("admin/formVacancies")?>">Add Vacancy</a>
                <!-- <a type="button" class="btn btn-gradient-primary btn-rounded btn-fw" href="">Add Vacancy</a> -->
                  <!-- <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i> -->
                </li>
              </ul>
            </nav>
          </div>

          <div class="row">
            <div class="col-md-12">
            <table id="table_id" class="display">
              <thead>
                  <tr>
                      <th>No</th>
                      <th>Vacancies Title</th>
                      <th>Unit</th>
                      <th>Level</th>
                      <th>Closing Date</th>
                      <th>QTY</th>
                      <th>Status</th>
                      <th>Action</th>
                  </tr>
              </thead>
              <tbody>
              <?php if(!empty($vacancies->result())){
                $no=1;
                foreach($vacancies->result() as $data) {
                ?>
                  <tr>
                      <td><?=$no;?></td>
                      <td><?=$data->position;?></td>
                      <td><?=$data->unit;?></td>
                      <td><?=$data->level;?></td>
                      <td><?=$data->deadline;?></td>
                      <td><?=$data->quantity;?></td>
                      <td><?=$data->status;?></td>
                      <td> 
                      <?php if($data->action == "input_interview_schedule"){?>
                      <a class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" href="<?=base_url("admin/inputSchedule/")?><?=$data->id_vacancies?>"><?=$data->action;?></a>  
                      <?php } else if($data->action == "input_result_interview") {?>
                      <a class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" href="<?=base_url("admin/resultInterview/")?><?=$data->id_vacancies?>"><?=$data->action;?></a>  
                      <?php }else { ?>
                      <a><?=$data->action;?></a>
                      <?php } ?>
                    </td>

                  </tr>
              <?php  $no++; } } else { ?>
                  <tr>
                  <td colspan="8" align="center">Data Is Empty</td>
                  </tr>
                <?php } ?>
              
          </table>
            </div>
            
          </div>
        
        </div>
