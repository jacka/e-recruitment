
     <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
              <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>
              </span>
              General Test Online
            </h3>
            <div class="flash-data" data-vacancy="<?php echo $this->session->flashdata('item'); ?>"></div>

           
            <!-- <nav aria-label="breadcrumb">
              <ul class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">
                  <a class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" href="<?=base_url("admin/formVacancies")?>">Add Vacancy</a>
                </li>
              </ul>
            </nav> -->
          </div>

          <div class="row">
            <div class="col-md-12">


            <table id="table_id" class="display">
              <thead>
                  <tr>
                      <th>No</th>
                      <th>Soal</th>
                      <th>A</th>
                      <th>B</th>
                      <th>C</th>
                      <th>D</th>
                      <th>E</th>
                      <th>Jawaban</th>
                      <th>Action</th>
                  </tr>
              </thead>
              <tbody>
              <?php if(!empty($test_online->result())){
                $no=1;
                foreach($test_online->result() as $data) {
                ?>
                  <tr>
                      <td><?=$no;?></td>
                      <td><?=$data->soal;?></td>
                      <td><?=$data->pilihan_a;?></td>
                      <td><?=$data->pilihan_b;?></td>
                      <td><?=$data->pilihan_c;?></td>
                      <td><?=$data->pilihan_d;?></td>
                      <td><?=$data->pilihan_e;?></td>
                      <td align="center"><strong><?=$data->jawaban;?></strong></td>
                      <td><a class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" href="<?=base_url("admin/formEditQuestion/")?><?=$data->id_soal?>">Edit</a>  </td>
                  </tr>
              <?php  $no++; } } else { ?>
                  <tr>
                  <td colspan="8" align="center">Data Is Empty</td>
                  </tr>
                <?php } ?>
              
          </table>
            </div>
            
          </div>
        
        </div>
