 <!-- partial -->
 <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
              Job Vacancy
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Forms</a></li>
              </ol>
            </nav>
          </div>
          <div class="row">
            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                <?php
                  echo form_open('admin/formVacancies');
                 ?>
                    <p class="card-description">
                      Add Vacancy
                    </p>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="col-sm-12">
                            <label class="col-form-label">Unit <label style="color:red">*</label></label> 
                            <select class="form-control" name="unit" required>
                              <option value="">Select your unit</option>
                              <option>IT/Computer - Software</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="col-sm-12">
                            <label class="col-form-label">Position <label style="color:red">*</label></label> 
                            <select class="form-control" name="position" required>
                            <option value="">Select your position</option>
                              <option>Senior Programmer</option>
                              <option>Middle Programmer</option>
                              <option>Junior Programmer</option>
                              <option>IT Support</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="col-sm-12">
                            <label class="col-form-label">Sub Unit <label style="color:red">*</label></label> 
                            <select class="form-control" name="sub_unit" required>
                              <option value="">Select your sub unit</option>
                              <option>Data Scientist</option>
                              <option>Functional Consultant/Business Analyst</option>
                              <option>IT Auditor</option>
                              <option>Product Management</option>
                              <option>Researcher</option>
                              <option>Software Architect</option>
                              <option>Software Engineer/Programmer</option>
                              <option>Software Quality Assurance</option>
                              <option>Software Security</option>
                              <option>Software/Application Trainer</option>
                              <option>Supervisor/Team Lead</option>
                              <option>System Analyst</option>
                              <option>Technical Writer</option>
                              <option>UI/UX Designer</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="col-sm-12">
                            <label class="col-form-label">Deadline <label style="color:red">*</label></label> 
                            <input type="date" class="form-control" placeholder="dd/mm/yyyy" name="deadline" required/>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="col-sm-12">
                            <label class="col-form-label">Location <label style="color:red">*</label></label>
                            <select class="form-control" name="location" required>
                              <option value="">Select your location</option>
                              <option>Jakarta Pusat</option>
                              <option>Jakarta Barat</option>
                              <option>Jakarta Selatan</option>
                              <option>Jakarta Utara</option>
                            </select>
                          </div>
                        </div>
                      </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <div class="col-sm-12">
                              <label class="col-form-label">Quantity <label style="color:red">*</label></label>
                              <input type="text" class="form-control" name="quantity" required/>
                            </div>
                          </div>
                        </div>
                    </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="col-sm-12">
                            <label class="col-form-label">Type <label style="color:red">*</label></label> 
                            <select class="form-control" name="type" required>
                              <option value="">Select your type</option>
                              <option>Full-Time</option>
                              <option>Internship</option>
                              <option>Part-Time</option>
                              <option>Temporary</option>
                              <option>Contract</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="col-sm-12">
                            <label class="col-form-label">Salary <label style="color:red">*</label></label>
                            <input type="text" class="form-control" name="salary" required/>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="col-sm-12">
                            <label class="col-form-label">Level <label style="color:red">*</label></label> 
                            <select class="form-control" name="level" required>
                              <option value="">Select your level</option>
                              <option>CEO / GM / Director / Senior Manager</option>
                              <option>Manager / Assistant Manager</option>
                              <option>Supervisor / Coordinator</option>
                              <option>Staff (non-management & non-supervisor)r</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="col-sm-12">
                            <label class="col-form-label">Minimal Education <label style="color:red">*</label></label> 
                            <select class="form-control" name="education" required>
                              <option value="">Select your education</option>
                              <option>SLTA</option>
                              <option>Sarjana</option>
                              <option>Magister</option>
                              <option>Doktor</option>
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="col-sm-12">
                           <label class="co-form-label">Responsibillities <label style="color:red">*</label></label> 
                           <textarea class="form-control" id="exampleTextarea1" rows="4" name="responsibillities" required></textarea>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="col-sm-12">
                             <label class="co-form-label">Requirements <label style="color:red">*</label></label> 
                             <textarea class="form-control" id="exampleTextarea1" rows="4" name="requirements" required></textarea>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">

                          <label class="col-sm-12 col-form-label">Experience <label style="color:red">*</label></label> 
                          <div class="form-group row" style="padding-left:20px">
                          <div class="col-sm-6">
                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="experience" value="true" checked>
                                Yes
                              </label>
                            </div>
                          </div>
                          <div class="col-sm-6">
                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="experience" value="false">
                                No
                              </label>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>

                    <div class="row">
                    <div class="col-md-12" style="padding: 40px;">
                      <button type="submit" class="btn btn-info btn-fw" style="width:100%" name="submit">Submit</button>
                    </div>
                  </div>

                  
                  </form>

                  

                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
