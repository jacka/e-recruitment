
<?php
$currentPage = $this->uri->segment('3');
 ?>
 <!-- partial -->
 <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
              General Question
            </h3>
            <nav aria-label="breadcrumb">
              <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Forms</a></li>
              </ol>
            </nav>
          </div>

          <?php
            echo form_open("admin/formEditQuestion/$currentPage");
           ?>

          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <div class="col-sm-12">
                 <label class="co-form-label">Soal <label style="color:red">*</label></label>
                 <input type="text" class="form-control" name="id_soal" value=<?=$test->id_soal?> style="display:none" />
                 <textarea class="form-control" id="exampleTextarea1" rows="6" name="soal" required><?=$test->soal?></textarea>
                </div>
              </div>
            </div>
          </div>

                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="col-sm-12">
                            <label class="col-form-label">Pilihan A <label style="color:red">*</label></label>
                            <textarea class="form-control" id="exampleTextarea1" rows="4" name="pil_a" required><?=$test->pilihan_a?></textarea>
                          </div>
                        </div>
                      </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <div class="col-sm-12">
                          <label class="col-form-label">Pilihan B <label style="color:red">*</label></label>
                            <textarea class="form-control" id="exampleTextarea1" rows="4" name="pil_b" required><?=$test->pilihan_b?></textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                        <div class="col-sm-12">
                          <label class="col-form-label">Pilihan C <label style="color:red">*</label></label>
                            <textarea class="form-control" id="exampleTextarea1" rows="4" name="pil_c" required><?=$test->pilihan_c?></textarea>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group">
                        <div class="col-sm-12">
                          <label class="col-form-label">Pilihan D <label style="color:red">*</label></label>
                            <textarea class="form-control" id="exampleTextarea1" rows="4" name="pil_d" required><?=$test->pilihan_d?></textarea>
                        </div>
                      </div>
                    </div>
                </div>

                <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <div class="col-sm-12">
                            <label class="col-form-label">Pilihan E <label style="color:red">*</label></label>
                              <textarea class="form-control" id="exampleTextarea1" rows="4" name="pil_e" required><?=$test->pilihan_e?></textarea>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">

                          <label class="col-sm-12 col-form-label">Jawaban <label style="color:red">*</label></label>
                          <div class="form-group row" style="padding-left:20px">
                          <div class="col-sm-2">
                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="jawaban" value="A" <?php if($test->jawaban == "A") echo "checked"; ?>>
                                A
                              </label>
                            </div>
                          </div>
                          <div class="col-sm-2">
                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="jawaban" value="B" <?php if($test->jawaban == "B") echo "checked"; ?>>
                                B
                              </label>
                            </div>
                          </div>
                          <div class="col-sm-2">
                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="jawaban" value="C" <?php if($test->jawaban == "C") echo "checked"; ?>>
                                C
                              </label>
                            </div>
                          </div>
                          <div class="col-sm-2">
                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="jawaban" value="D" <?php if($test->jawaban == "D") echo "checked"; ?>>
                                D
                              </label>
                            </div>
                          </div>
                          <div class="col-sm-2">
                            <div class="form-check">
                              <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="jawaban" value="E" <?php if($test->jawaban == "E") echo "checked"; ?>>
                                E
                              </label>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>

                    <div class="row">
                    <div class="col-md-12" style="padding: 40px;">
                      <button type="submit" class="btn btn-info btn-fw" style="width:100%" name="submit">Submit</button>
                    </div>
                  </div>


                  </form>



                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
