<!-- partial:partials/_sidebar.html -->
<nav class="sidebar sidebar-offcanvas" id="sidebar">
        <ul class="nav">
          <li class="nav-item nav-profile">
            <a href="#" class="nav-link">
              <div class="nav-profile-image">
                <img src="<?php echo base_url('assets/') ?>siluet.png" alt="profile">
                <span class="login-status online"></span> <!--change to offline or busy as needed-->              
              </div>
              <div class="nav-profile-text d-flex flex-column">
                <span class="font-weight-bold mb-2"><?php echo $this->session->userdata('username'); ?></span>
                <span class="text-secondary text-small">HRD</span>
              </div>
              <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('admin/') ?>">
              <span class="menu-title">Dashboard</span>
              <i class="mdi mdi-home menu-icon"></i>
            </a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="<?php// echo base_url('admin/listApplicant') ?>">
              <span class="menu-title">Applicant</span>
              <i class="mdi mdi-zip-box menu-icon"></i>
            </a>
          </li> -->
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('admin/listJob') ?>">
              <span class="menu-title">To Do List</span>
              <i class="mdi mdi-format-list-bulleted menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('admin/listReport') ?>">
              <span class="menu-title">Report</span>
              <i class="mdi mdi-file-check menu-icon"></i>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo base_url('admin/testOnline') ?>"> 
              <span class="menu-title">Tes Online</span>
              <i class="mdi mdi-comment-question-outline menu-icon"></i>
            </a>
          </li>
        </ul>
      </nav>
<!-- partial -->
