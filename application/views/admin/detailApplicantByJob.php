
     <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
              <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>
              </span>
              Detail Of Applicant By Job
            </h3>
            <div class="flash-data" data-vacancy="<?php echo $this->session->flashdata('item'); ?>"></div>

            <!-- <nav aria-label="breadcrumb">
              <ul class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">
                  <a class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" href="<?=base_url("admin/formVacancies")?>">Add Vacancy</a>
                </li>
              </ul>
            </nav> -->
          </div>
          <div class="row">
            <div class="col-md-12">
            <table id="table_id" class="display">
              <thead>
                  <tr>
                      <th>No</th>
                      <th>Username</th>
                      <th>Full Name</th>
                      <th>Place of Birth</th>
                      <th>Date Of Birth</th>
                      <th>Gender</th>
                      <th>Height</th>
                      <th>Weight</th>
                      <th>Religion</th>
                      <th>Phone Number</th>
                      <th>Address</th>
                      <th>Level</th>
                      <th>Study</th>
                      <th>Test Score</th>
                  </tr>
              </thead>
              <tbody>
              <?php if(!empty($applicant->result())){
                $no=1;
                foreach($applicant->result() as $data) {
                ?>
                  <tr>
                      <td><?=$no;?></td>
                      <td><?=$data->email;?></td>
                      <td><?=$data->fullName;?></td>
                      <td><?=$data->placeBirth;?></td>
                      <td><?=date("d-m-Y", strtotime($data->dateBirth));?></td>
                      <td><?=$data->gender;?></td>
                      <td><?=$data->height;?></td>
                      <td><?=$data->weight;?></td>
                      <td><?=$data->religion;?></td>
                      <td><?=$data->phoneNumber;?></td>
                      <td><?=$data->address;?></td>
                      <td><?=$data->educationLevel;?></td>
                      <td><?=$data->educationInstitution;?></td>
                      <td><?=$data->score;?></td>
                  </tr>
              <?php  $no++; } } else { ?>
                  <tr>
                  <td colspan="8" align="center">Data Is Empty</td>
                  </tr>
                <?php } ?>
              
          </table>
            </div>
            
          </div>
        
        </div>
