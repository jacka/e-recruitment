

<?php
$data = $vacancy->row();

// var_dump($applicant->result());
?>
     <div class="main-panel">
        <div class="content-wrapper">
          <div class="page-header">
            <h3 class="page-title">
              <span class="page-title-icon bg-gradient-primary text-white mr-2">
                <i class="mdi mdi-home"></i>
              </span>
              Result Interview
            </h3>
            <div class="flash-data" data-vacancy="<?php echo $this->session->flashdata('item'); ?>"></div>

            <nav aria-label="breadcrumb">
              <ul class="breadcrumb">
                <li class="breadcrumb-item active" aria-current="page">
                  <a class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" href="<?=base_url("admin/formVacancies")?>">Add Vacancy</a>
                <!-- <a type="button" class="btn btn-gradient-primary btn-rounded btn-fw" href="">Add Vacancy</a> -->
                  <!-- <i class="mdi mdi-alert-circle-outline icon-sm text-primary align-middle"></i> -->
                </li>
              </ul>
            </nav>
          </div>

          <div>

          <?php
          echo form_open("admin/resultInterview/$data->id_vacancies");
          ?>

          <div class="row">
          <div class="col-lg-12 grid-margin stretch-card">
              <div class="card">
                <div class="card-body">
                  <h4 class="card-title">Detail Vacancy</h4>
                  <table class="table">
                      <tr>
                        <td>
                          1
                        </td>
                        <td>
                          Vacancy Id
                        </td>
                        <td>
                        <b> : </b>
                        </td>
                        <td>
                        <?=$data->id_vacancies?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          2
                        </td>
                        <td>
                        Unit
                        </td>
                        <td>
                        <b> : </b>
                        </td>
                        <td>
                        <?=$data->unit?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          3
                        </td>
                        <td>
                        Position
                        </td>
                        <td>
                        <b> : </b>
                        </td>
                        <td>
                        <?=$data->position?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          4
                        </td>
                        <td>
                        Location
                        </td>
                        <td>
                        <b> : </b>
                        </td>
                        <td>
                        <?=$data->location?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          5
                        </td>
                        <td>
                        Type
                        </td>
                        <td>
                          <b> : </b>
                        </td>
                        <td>
                        <?=$data->type?>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          6
                        </td>
                        <td>
                        Quantity
                        </td>
                        <td>
                          <b> : </b>
                        </td>
                        <td>
                        <?=$data->quantity?>
                        </td>
                      </tr>
                      
                  </table>
                </div>
              </div>
            </div>

          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-body">
            <table id="table_id" class="display">
              <thead>
                  <tr>
                      <th>No</th>
                      <th>Name</th>
                      <th>Gender</th>
                      <th>Date Of Birth</th>
                      <th>Education</th>
                      <th>Experience</th>
                      <th>Test Score</th>
                      <th>Communication</th>
                      <th>Appereance</th>
                      <th>Attitude</th>
                      <th>Action</th>
                      <th style="display:none">Email</th>
                      <th style="display:none">qty</th>
                  </tr>
              </thead>
              <tbody>

              <?php
              if(!empty($applicant->result())){
                $no=1;
                foreach($applicant->result() as $data) {
                ?>
                  <tr>
                      <td><?=$no;?></td>
                      <td><?=$data->fullName;?></td>
                      <td><?=$data->gender;?></td>
                      <td><?=$data->dateBirth;?></td>
                      <td><?=$data->educationLevel;?></td>
                      <td><?=$data->institutionName;?></td>
                      <td><?=$data->score;?></td>
                      <td>
                      <select class="form-control" name="communication" required>
                              <option>Very Good</option>
                              <option>Good</option>
                              <option>Bad</option>
                            </select>
                      </td>
                      <td>
                      <select class="form-control" name="appereance" required>
                              <option>Very Good</option>
                              <option>Good</option>
                              <option>Bad</option>
                            </select>
                      </td>
                      <td>
                      <select class="form-control" name="attitude" required>
                              <option>Very Good</option>
                              <option>Good</option>
                              <option>Bad</option>
                            </select>
                      </td>
                      <td style="text-align:center"> 
                      <label class="form-check-label text-muted">
                        <input type="checkbox" class="form-check-input" name="check[]" value=<?=$data->id_apply_job?> >
                        Acc
                      </label>
                      </td>
                      <!-- <td style="display:none">  <input type="text" name="idApply[]" value=<?=$data->id_apply_job?> > </td> -->
                      <td style="display:none">  <input type="text" name="score[]" value=<?=$data->score?> > </td>
                      <td style="display:none">  <input type="text" name="email[]" value=<?=$data->email?> > </td>
                      <td style="display:none">  <input type="text" name="qty" value=<?=$vacancy->row()->quantity?> > </td>
                  </tr>
              <?php  $no++; } } else { ?>
                  <tr>
                  <td colspan="8" align="center">Data Is Empty</td>
                  </tr>
                <?php } ?>

          </table>
        </div>
        </div>


            </div>

          </div>

          <div style="display:flex;justify-content: flex-end;margin-top:20px">
            <button type="submit" class="btn btn-gradient-primary btn-fw" name="submit">Send Notification</button>
          </div>
        </form>
        </div>


        </div>
