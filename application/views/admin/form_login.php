<!-- <?php
  //echo form_open('admin/login');
  ?>

  <input type="text" name="username" placeholder="username">
  <input type="password" name="password" placeholder="password">
  <button type="submit" name="submit">Login</button>
 </form> -->
 
 <!DOCTYPE html>
 <html lang="en">
 
 <head>
   <!-- Required meta tags -->
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <title>E - Recruitment</title>
   <!-- plugins:css -->
   <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>vendors/iconfonts/mdi/css/materialdesignicons.min.css">
   <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>vendors/css/vendor.bundle.base.css">
   <!-- endinject -->
   <!-- plugin css for this page -->
   <!-- End plugin css for this page -->
   <!-- inject:css -->
   <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>css/style.css">
   <!-- endinject -->
   <link rel="shortcut icon" href="<?php echo base_url('assets/admin/') ?>images/favicon.png" />
 </head>
 
 <body>
   <div class="container-scroller">
     <div class="container-fluid page-body-wrapper full-page-wrapper">
       <div class="content-wrapper d-flex align-items-center auth">
         <div class="row w-100">
           <div class="col-lg-4 mx-auto">
             <div class="auth-form-light text-left p-5">
 
               <div class="flash-data" data-login="<?php echo $this->session->flashdata('item'); ?>"></div>
               <h2 align="center">E - Recruitment</h4>
               <h4 align="center">Administrator</h3>
               
               <!-- <form class="pt-3"> -->
                 <?php
                  echo form_open('admin/login');
                 ?>
 
                 <label class="font-weight-light">Username</label>
                 <div class="form-group">
                   <input type="username" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Username" name="username">
                 </div>
 
                 <label class="font-weight-light">Password</label>
                 <div class="form-group">
                   <input type="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password" name="password">
                 </div>
                 <div class="mt-3">
                   <button class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" type="submit" name="submit">Login</button>
 
                   <!-- <a class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" href="../../index.html">SIGN IN</a> -->
                 </div>
                  </form>
              
                
               <!-- </form> -->
             </div>
           </div>
         </div>
       </div>
       <!-- content-wrapper ends -->
     </div>
     <!-- page-body-wrapper ends -->
   </div>
   <!-- container-scroller -->
   <!-- plugins:js -->
   <script src="<?php echo base_url('assets/admin/') ?>vendors/js/vendor.bundle.base.js"></script>
   <script src="<?php echo base_url('assets/admin/') ?>vendors/js/vendor.bundle.addons.js"></script>
   <!-- endinject -->
   <!-- inject:js -->
   <script src="<?php echo base_url('assets/admin/') ?>js/off-canvas.js"></script>
   <script src="<?php echo base_url('assets/admin/') ?>js/misc.js"></script>
   <!-- endinject -->
 
   <!-- js sweet alert admin -->
 <script type="text/javascript" src="<?php echo base_url('assets/admin') ?>/js/admin.js"></script>
 <!-- tutup sweet alert admin js -->
 
 <!-- js custom admin -->                                                                       
 <script type="text/javascript" src="<?php echo base_url('assets/admin/js/sweetalert/') ?>sweetalert2.all.min.js"></script>
 <!-- tutup custom admin js -->
 
 </body>
 
 </html>
 