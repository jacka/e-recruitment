<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>E-Recruitment</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/css/bootstrap.min.css" >
    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/fonts/font-awesome.min.css">
    <!-- Icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/fonts/simple-line-icons.css">
    <!-- Slicknav -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/css/slicknav.css">
    <!-- Menu CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/css/menu_sideslide.css">
    <!-- Slider CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/css/slide-style.css">
    <!-- Nivo Lightbox -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/css/nivo-lightbox.css" >
    <!-- Animate -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/css/animate.css">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/css/main.css">
    <!-- Responsive Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/css/responsive.css">

  </head>
  <body>
  <?php
  $currentPage = $this->uri->segment('2');
  ?>
    <!-- Header Area wrapper Starts -->
    <header id="header-wrap">
      <!-- Navbar Start -->
      <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar indigo">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              <span class="icon-menu"></span>
              <span class="icon-menu"></span>
              <span class="icon-menu"></span>
            </button>
            <a href="<?=base_url('user')?>" class="navbar-brand"> <img src="<?php echo base_url('assets/') ?>logoHeader.png"></a>
          </div>
          
          <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="onepage-nev navbar-nav mr-auto w-100 justify-content-end clearfix">
              <li class="nav-item <?php if(empty($currentPage)) echo "active"; ?>"">
                <a class="nav-link active" href="<?=base_url('user') ?>">
                  Home
                </a>
              </li>
              <li class="nav-item <?php if(!empty($currentPage)) echo "active"; ?>">
                <a class="nav-link" href="<?=base_url('user/jobVacancy') ?>">
                  Job Vacancies 
                </a>
              </li>
              <li class="nav-item">
              <div class="dropdown">
                  <a class="nav-link dropdown-toggle" style="cursor: pointer;" data-toggle="dropdown">
                   Welcome, <?php echo $this->session->userdata('username'); ?>
                  </a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?=base_url('user/logout') ?>">Logout</a>
                  </div>
              </div>
              </li>
              <!--<li class="nav-item">
                <a class="nav-link" href="#resume">
                  Resume
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#portfolios">
                  Work
                </a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#contact">
                  Contact
                </a>
              </li> -->
            </ul>
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="onepage-nev mobile-menu">
          <li>
            <a href="#home">Home</a>
          </li>
          <li>
            <a href="#about">Job Vacancies</a>
          </li>
          <!-- <li>
            <a href="#services">Services</a>
          </li>
          <li>
            <a href="#resume">resume</a>
          </li>
          <li>
            <a href="#portfolio">Work</a>
          </li>
          <li>
            <a href="#contact">Contact</a>
          </li> -->
        </ul>
        <!-- Mobile Menu End -->
      </nav>
      <!-- Navbar End -->

    </header>