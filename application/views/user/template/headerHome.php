<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>E-Recruitment</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/css/bootstrap.min.css" >
    <!-- Fonts -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/fonts/font-awesome.min.css">
    <!-- Icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/fonts/simple-line-icons.css">
    <!-- Slicknav -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/css/slicknav.css">
    <!-- Menu CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/css/menu_sideslide.css">
    <!-- Slider CSS -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/css/slide-style.css">
    <!-- Nivo Lightbox -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/css/nivo-lightbox.css" >
    <!-- Animate -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/css/animate.css">
    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/css/main.css">
    <!-- Responsive Style -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/user/') ?>assets/css/responsive.css">

  </head>
  <body>

    <!-- Header Area wrapper Starts -->
    <header id="header-wrap">
      <!-- Navbar Start -->
      <nav class="navbar navbar-expand-lg fixed-top scrolling-navbar indigo">
        <div class="container">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
              <span class="icon-menu"></span>
              <span class="icon-menu"></span>
              <span class="icon-menu"></span>
            </button>
            <a href="<?=base_url('user')?>" class="navbar-brand"> <img src="<?php echo base_url('assets/') ?>logoHeader.png"></a>
          </div>
          <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="onepage-nev navbar-nav mr-auto w-100 justify-content-end clearfix">
              <li class="nav-item active">
              <div class="dropdown">
                  <a class="nav-link dropdown-toggle" style="cursor: pointer;" data-toggle="dropdown">
                   Login
                  </a>
                  <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?php echo base_url('user/login'); ?>">Login By User</a>
                    <a class="dropdown-item" href="<?php echo base_url('admin/login'); ?>">Login By Admin</a>
                  </div>
              </div>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('user/register'); ?>">
                  Register
                </a>
              </li>
            </ul>
          </div>
        </div>

        <!-- Mobile Menu Start -->
        <ul class="onepage-nev mobile-menu">
          <li>
            <a href="<?php echo base_url('user/login'); ?>">Login</a>
          </li>
          <li>
            <a href="<?php echo base_url('user/register'); ?>">Register</a>
          </li>
        </ul>
        <!-- Mobile Menu End -->
      </nav>
      <!-- Navbar End -->

      <!-- Hero Area Start -->
      <div id="hero-area" class="hero-area-bg">
        <div class="overlay"></div>
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-sm-12 text-center">
              <div class="contents">
                <h5 class="script-font wow fadeInUp" data-wow-delay="0.2s">PT Megah Mas Prima</h5>
                <h2 class="head-title wow fadeInUp" data-wow-delay="0.4s">E-Recruitment</h2>
                <p class="script-font wow fadeInUp" data-wow-delay="0.6s">Bergabung Bersama Kami untuk Mendapatkan Karir Lebih Baik</p>
                <ul class="social-icon wow fadeInUp" data-wow-delay="0.8s">
                  <li>
                    <a class="facebook" href="#"><i class="icon-social-facebook"></i></a>
                  </li>
                  <li>
                    <a class="twitter" href="#"><i class="icon-social-twitter"></i></a>
                  </li>
                  <li>
                    <a class="instagram" href="#"><i class="icon-social-instagram"></i></a>
                  </li>
                  <li>
                    <a class="linkedin" href="#"><i class="icon-social-linkedin"></i></a>
                  </li>
                  <li>
                    <a class="google" href="#"><i class="icon-social-google"></i></a>
                  </li>
                </ul>
                <div class="header-button wow fadeInUp" data-wow-delay="1s">
                  <a href="<?php echo base_url('user/register'); ?>" class="btn btn-common">Daftar Sekarang</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Hero Area End -->

    </header>