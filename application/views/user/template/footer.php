
    <!-- Footer Section Start -->
    <footer class="footer-area section-padding">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="footer-text text-center wow fadeInDown" data-wow-delay="0.3s">
              <ul class="social-icon">
                <li>
                  <a class="facebook" href="#"><i class="icon-social-facebook"></i></a>
                </li>
                <li>
                  <a class="twitter" href="#"><i class="icon-social-twitter"></i></a>
                </li>
                <li>
                  <a class="instagram" href="#"><i class="icon-social-instagram"></i></a>
                </li>
                <li>
                  <a class="instagram" href="#"><i class="icon-social-linkedin"></i></a>
                </li>
                <li>
                  <a class="instagram" href="#"><i class="icon-social-google"></i></a>
                </li>
              </ul>
              <p>Copyright © 2018 UIdeck All Right Reserved</p>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <!-- Footer Section End -->

    <!-- Go to Top Link -->
    <a href="#" class="back-to-top">
      <i class="icon-arrow-up"></i>
    </a>

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="<?php echo base_url('assets/user/') ?>assets/js/jquery-min.js"></script>
    <script src="<?php echo base_url('assets/user/') ?>assets/js/popper.min.js"></script>
    <script src="<?php echo base_url('assets/user/') ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url('assets/user/') ?>assets/js/jquery.mixitup.js"></script>
    <script src="<?php echo base_url('assets/user/') ?>assets/js/jquery.counterup.min.js"></script>
    <script src="<?php echo base_url('assets/user/') ?>assets/js/waypoints.min.js"></script>
    <script src="<?php echo base_url('assets/user/') ?>assets/js/wow.js"></script>
    <script src="<?php echo base_url('assets/user/') ?>assets/js/jquery.nav.js"></script>
    <script src="<?php echo base_url('assets/user/') ?>assets/js/jquery.easing.min.js"></script>  
    <script src="<?php echo base_url('assets/user/') ?>assets/js/nivo-lightbox.js"></script>
    <script src="<?php echo base_url('assets/user/') ?>assets/js/jquery.slicknav.js"></script>
    <script src="<?php echo base_url('assets/user/') ?>assets/js/main.js"></script>
    <script src="<?php echo base_url('assets/user/') ?>assets/js/form-validator.min.js"></script>
    <script src="<?php echo base_url('assets/user/') ?>assets/js/contact-form-script.min.js"></script>
    <script src="<?php echo base_url('assets/user/') ?>assets/js/map.js"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/user') ?>/assets/js/user.js"></script>
 <!-- js custom admin -->                                                                       
 <script type="text/javascript" src="<?php echo base_url('assets/admin/js/sweetalert/') ?>sweetalert2.all.min.js"></script>
 <!-- tutup custom admin js -->
  </body>
</html>
