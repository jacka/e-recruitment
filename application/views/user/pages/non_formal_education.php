
    <!-- About Section Start -->
    <section id="non-formal" class="section-padding">
    <h2 class="section-title wow flipInX" data-wow-delay="0.4s">Non Formal Education</h2>

      <div class="container">
        <div class="row">

        <?php if(!empty($non_formal_education->row())){?>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="img-thumb wow fadeInLeft" data-wow-delay="0.3s">
              <img class="img-fluid" src="<?=base_url()?><?=$non_formal_education->row()->imgCertificate;?>" style="width: 580px;" alt="">
            </div>
          </div> 

          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="wow fadeInRight" data-wow-delay="0.3s">
              <div class="about-profile">
              <table  class="table" >
                        <tr>
                            <td>Training Name</td>
                            <td>:</td>
                            <td><?=$non_formal_education->row()->trainingName;?></td>
                        </tr>
                        <tr>
                            <td>Training Scope</td>
                            <td>:</td>
                            <td><?=$non_formal_education->row()->trainingScope;?></td>
                        </tr>
                        <tr>
                            <td>Training Institution</td>
                            <td>:</td>
                            <td><?=$non_formal_education->row()->trainingInstitution;?></td>
                        </tr>
                        <tr>
                            <td>Training Certificate Number</td>
                            <td>:</td>
                            <td><?=$non_formal_education->row()->certificateNumber;?></td>
                        </tr>
                        <tr>
                            <td>Start Date</td>
                            <td>:</td>
                            <td><?=$non_formal_education->row()->startDate;?></td>
                        </tr>
                        <tr>
                            <td>End Date</td>
                            <td>:</td>
                            <td><?=$non_formal_education->row()->endDate;?></td>
                        </tr>
                </table>
              </div>
              <a href="<?=base_url('user/formFormalEducation')?>" class="btn btn-common"><i class="icon-paper-clip"></i>Ubah Data</a>
              <a href="#" class="btn btn-danger"><i class="icon-speech"></i>Hapus Data</a>
            </div>
          </div>  
          <?php } else {?>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="justify-content:center;display:flex;flex-direction: column;">
              <h3 style="text-align: center;">Data Is Empty</h3>
              <a href="<?=base_url('user/formNonFormalEducation')?>" class="btn btn-common"><i class="icon-paper-clip"></i>Add Data</a>
          </div>
        <?php } ?>


        </div>
      </div>
    </section>
    <!-- About Section End -->
