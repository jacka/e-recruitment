
    <!-- About Section Start -->
    <section id="personal-data" class="section-padding">
    <h2 class="section-title wow flipInX" data-wow-delay="0.4s">Personal Data</h2>

      <div class="container">
        <div class="row">

        <?php if(!empty($data_personal->row())){?>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="img-thumb wow fadeInLeft" data-wow-delay="0.3s">
              <img class="img-fluid" src="<?=base_url()?><?=$data_personal->row()->gambarProfil;?>" style="width: 580px;">
            </div>
          </div> 

          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="wow fadeInRight" data-wow-delay="0.3s">
              <div class="about-profile">
              <table  class="table" >
                        <tr>
                            <td>Full Name</td>
                            <td>:</td>
                            <td><?=$data_personal->row()->fullName;?></td>
                        </tr>
                        <tr>
                            <td>Place Of Birth</td>
                            <td>:</td>
                            <td><?=$data_personal->row()->placeBirth;?></td>
                        </tr>
                        <tr>
                            <td>Date Of Birth</td>
                            <td>:</td>
                            <td><?=$data_personal->row()->placeBirth;?></td>
                        </tr>
                        <tr>
                            <td>Gender</td>
                            <td>:</td>
                            <td><?=$data_personal->row()->gender;?></td>
                        </tr>
                        <tr>
                            <td>Height</td>
                            <td>:</td>
                            <td><?=$data_personal->row()->height;?></td>
                        </tr>
                        <tr>
                            <td>Weight</td>
                            <td>:</td>
                            <td><?=$data_personal->row()->weight;?></td>
                        </tr>
                        <tr>
                            <td>Religion</td>
                            <td>:</td>
                            <td><?=$data_personal->row()->religion;?></td>
                        </tr>
                        <tr>
                            <td>Phone Number</td>
                            <td>:</td>
                            <td><?=$data_personal->row()->phoneNumber;?></td>
                        </tr>
                        <tr>
                            <td>Address</td>
                            <td>:</td>
                            <td><?=$data_personal->row()->address;?></td>
                        </tr>

                </table>
              </div>
              <!-- <a href="<?=base_url('user/formPersonalData')?>" class="btn btn-common"><i class="icon-paper-clip"></i>Ubah Data</a> -->
              <a href="<?=base_url('user/deletePersonalData/')?>" class="btn btn-danger"><i class="icon-speech"></i>Hapus Data</a> 
            </div>
          </div>  
          <?php } else {?>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="justify-content:center;display:flex;flex-direction: column;">
              <h3 style="text-align: center;">Data Is Empty</h3>
              <a href="<?=base_url('user/formPersonalData')?>" class="btn btn-common"><i class="icon-paper-clip"></i>Add Data</a>
          </div>
        <?php } ?>
 
        </div>
      </div>
    </section>
    <!-- About Section End -->
