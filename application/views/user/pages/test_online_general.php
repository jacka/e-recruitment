<?php
$dataTest = $test->row();
$nextPage = ((int)$this->uri->segment('3') + 1);
$currentPage = $this->uri->segment('3');
?>

<!-- Resume Section Start -->
 <div id="resume" class="section-padding" style="margin-top:50px">
      <div class="container">
        <div class="row">
        
        <div class="col-md-2 col-lg-2 col-sm-12">
              <div class="contact-block">
                <h2>Question</h2>
                <div class="row">          
          <div class="col-md-12">
            <!-- Portfolio Controller/Buttons -->
            <div class="controls text-center">
              <a class="filter active btn btn-common" data-filter="all" style="width:100%">
                General 
              </a>
            </div>
            <!-- Portfolio Controller/Buttons Ends-->
          </div>
        </div>
              </div>
            </div>

            <div class="col-md-10 col-lg-10 col-sm-12" style="margin-top:55px">
                <h2>General Question</h2>
                <div class="flash-data" data-test="<?php echo $this->session->flashdata('item');?>"></div>
                <div class="footer-right-contact" style="border-color: #fff;">
                  <div class="row">
                  <div class="col-md-12 col-lg-12 col-xs-12">
                      <div>
                        <p style="margin-bottom:20px"><b><?=$dataTest->id_soal?>.</b> <?=$dataTest->soal?></p>

                        <?php
                          echo form_open("user/testOnlineGeneral/$currentPage");
                        ?>

                      <input type="text" name="id_soal" value=<?=$dataTest->id_soal?> style="display:none">
                      <input type="text" name="jawabanTrue" value=<?=$dataTest->jawaban?> style="display:none">

                      <div style="margin-left:20px">
                      <label>
                                <input type="radio" class="form-check-input" name="jawaban" value="A" >
                                A. <?=$dataTest->pilihan_a?>
                              </label>
                      </div>

                      <div style="margin-left:20px">
                      <label>
                                <input type="radio" class="form-check-input" name="jawaban" value="B" >
                                B. <?=$dataTest->pilihan_b?>
                              </label>
                      </div>

                      <div style="margin-left:20px">
                      <label>
                                <input type="radio" class="form-check-input" name="jawaban" value="C" >
                                C. <?=$dataTest->pilihan_c?>
                              </label>
                      </div>

                      <div style="margin-left:20px">
                      <label>
                                <input type="radio" class="form-check-input" name="jawaban" value="D" >
                                D. <?=$dataTest->pilihan_d?>
                              </label>
                      </div>

                      <div style="margin-left:20px">
                      <label>
                                <input type="radio" class="form-check-input" name="jawaban" value="E" >
                                E. <?=$dataTest->pilihan_e?>
                              </label>
                      </div>
                      </div>

                  </div>
                </div>

                 <div class="controls text-center" style="display: flex;justify-content: space-between;margin-top:20px">
                  <a class="filter btn btn-common" style="background-color:#fff">
                    Back 
                  </a>

                  <button class="filter btn btn-common" type="submit" name="submit">
                    Next
                  </button>

                  <!-- <?php if((int)$this->uri->segment('3') == $sum_test->num_rows()){ ?>
                  <a class="filter btn btn-common">
                    Finish
                  </a>
                  <?php } else {?>
                    <button class="filter btn btn-common" type="submit" name="submit">
                    Next
                  </button>
                  <?php } ?> -->

                  </form>
            </div>

                </div>
              </div>
            </div>

        </div>
    </div>
    <!-- Resume Section End -->