 
 <!-- Resume Section Start -->
 <div id="resume" class="section-padding" style="margin-top:50px">
      <div class="container">
        <div class="row">
        
        <div class="col-md-6 col-lg-6 col-sm-12">
              <div class="contact-block">
                <h2>Subcribe</h2>
                <form id="contactForm">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group" style="display:none">
                        <input type="text" class="form-control" id="email" name="email" placeholder="email" required data-error="Please enter your name">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>
                    <div class="col-md-12">
                      <div class="form-group" style="display:none"> 
                        <textarea class="form-control" id="message" placeholder="Your Message" rows="5" data-error="Write your message" required></textarea>
                        <div class="help-block with-errors"></div>
                      </div>
                      
                      <p>dengan berlangganan anda dapat memperoleh informasi terbaru dari PT Megah Mas Prima</p>

                      <div class="submit-button">
                        <button class="btn btn-common" onClick="alert('Terimakasih, Anda sudah masuk ke daftar subcriber kami!')">Subcribe</button>
                        <div id="msgSubmit" class="h3 text-center hidden"></div> 
                        <div class="clearfix"></div> 
                      </div>
                    </div>
                  </div>            
                </form>
              </div>
            </div>

          <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="education wow fadeInRight" data-wow-delay="0.3s">
              <ul class="timeline">
                <li>
                  <i class="icon-briefcase"></i>
                  <h2 class="timelin-title">Job Vacancy</h2>
                </li>
                <?php if(!empty($vacancies->result())){
                $no=1;
                foreach($vacancies->result() as $data) {
                ?>
                <li>
                  <div class="content-text">
                    <h3 class="line-title"><?=$data->position;?></h3>
                    <span><?=$data->location;?></span>
                    <p><b>Deadline :</b><p>
                    <p class="line-text"><?=$data->deadline;?></p>

                    <a class="filter btn btn-common" style="margin-top:10px" data-filter=".design" href="<?=base_url("user/detailJobVacancy/{$data->id_vacancies}")?>">
                        detail 
                    </a>
                  </div>
                </li>
                <?php  $no++; } } else { ?>
                <li>
                  <div class="content-text">
                    <h3 class="line-title" style="text-align: center;">Tidak ada lowongan tersedia</h3>
                  </div>
                </li>
                <?php } ?>
              </ul>
            </div>
          </div>

        </div>
      </div>
    </div>
    <!-- Resume Section End -->