<!-- Resume Section Start -->
 <div id="resume" class="section-padding" style="margin-top:50px">
      <div class="container">
        <div class="row">
        
        <div class="col-md-2 col-lg-2 col-sm-12">
              <div class="contact-block">
                <h2>Question</h2>
                <div class="row">          
          <div class="col-md-12">
            <!-- Portfolio Controller/Buttons -->
            <div class="controls text-center">
              <a class="filter active btn btn-common" data-filter="all" style="width:100%">
                General 
              </a>
              <a class="filter btn btn-common" data-filter=".design" style="width:100%">
                Special 
              </a>
            </div>
            <!-- Portfolio Controller/Buttons Ends-->
          </div>
        </div>
              </div>
            </div>

            <div class="col-md-10 col-lg-10 col-sm-12" style="margin-top:55px">
                <h2>Result General Question</h2>
                <div class="footer-right-contact" style="border-color: #fff;">
                  <div class="row">
                  <div class="col-md-12 col-lg-12 col-xs-12">
                     <p style="font-size: x-large;">Your score:<?=$score?>. Congratulations! We will send you an email for further information</p>

                     <div class="controls text-center" style="display: flex;justify-content: space-between;margin-top:20px">
                  <a class="filter btn btn-common" style="background-color:#fff">
                    Back 
                  </a>

                  <a class="filter btn btn-common" href="<?=base_url("user")?>">
                    Back To Home
                  </a>

                  </div>
                </div>

                

                
            </div>

                </div>
              </div>
            </div>

        </div>
    </div>
    <!-- Resume Section End -->