<?php
$idJob = $this->uri->segment('3');
?>
 <!-- Resume Section Start -->
 <div id="resume" class="section-padding" style="margin-top:50px">
      <div class="container">
        <div class="row">
        
        <div class="col-md-4 col-lg-4 col-sm-12">
              <div class="contact-block">
                <h2>Subcribe</h2>
                <div class="flash-data" apply-job="<?php echo $this->session->flashdata('item');?>"></div>
                <form id="contactForm">
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group" style="display:none">
                        <input type="text" class="form-control" id="email" name="email" placeholder="email" required data-error="Please enter your name">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>
                    <div class="col-md-12">
                      <div class="form-group" style="display:none"> 
                        <textarea class="form-control" id="message" placeholder="Your Message" rows="5" data-error="Write your message" required></textarea>
                        <div class="help-block with-errors"></div>
                      </div>
                      
                      <p>dengan berlangganan anda dapat memperoleh informasi terbaru dari PT Megah Mas Prima</p>
                      
                      <div class="submit-button">
                        <button class="btn btn-common" onClick="alert('Terimakasih, Anda sudah masuk ke daftar subcriber kami!')">Subcribe</button>
                        <div id="msgSubmit" class="h3 text-center hidden"></div> 
                        <div class="clearfix"></div> 
                      </div>
                    </div>
                  </div>            
                </form>
              </div>
            </div>

            <div class="col-md-8 col-lg-8 col-sm-12">
              <div class="footer-right-area wow fadeIn">
                <h2>Job Detail</h2>
                <div class="footer-right-contact" style="border-color: #fff;">
                  <div class="single-contact">
                    <div class="contact-icon">
                      <i class="icon-check" style="color:#fff"></i>
                    </div>
                    <p><a href="mailto:hello@tom.com" style="font-weight:bold;">Responsibillites</a></p>
                    <ul>
                      <li><?=$vacancy->row()->responsibillities?></li>
                    </ul>
                  </div>
                  <div class="single-contact">
                    <div class="contact-icon">
                      <i class="icon-check" style="color:#fff"></i>
                    </div>
                    <p><a href="#" style="font-weight:bold;">Requirements</a></p>
                    <ul>
                      <li><?=$vacancy->row()->requirements?></li>
                    </ul>
                  </div>

                  <div class="single-contact">
                    <div class="contact-icon">
                      <i class="icon-check" style="color:#fff"></i>
                    </div>
                    <p><a href="#" style="font-weight:bold;">Job Information</a></p>
                    <div class="about-profile">
                      <ul class="admin-profile">
                        <li><span class="pro-title"> Education Level </span> <span class="pro-detail"><?=$vacancy->row()->education?></span></li>
                        <li><span class="pro-title"> Job Level </span> <span class="pro-detail"><?=$vacancy->row()->level?></span></li>
                        <li><span class="pro-title"> Job Type </span> <span class="pro-detail"><?=$vacancy->row()->type?></span></li>
                        <li><span class="pro-title"> Job Location </span> <span class="pro-detail"><?=$vacancy->row()->location?></span></li>
                        <li><span class="pro-title"> Work Experience </span> <span class="pro-detail"><?=$vacancy->row()->experience?></span></li>
                      </ul>
                    </div>
                  </div>

                  <div class="row">
                  <!-- Services item -->
                  <div class="col-md-12 col-lg-12 col-xs-12">
                      <div style="margin-top:20px">
                      <p>Dear, <b><?=$this->session->userdata('username')?></b></p>
                        <p>we will apply your job after an online trial, please be willing to set aside 30 minutes to take the test.
online test questions consist of 2 sessions, general session containing questions about basic knowledge and a special that containt questions related to the work you choose. if you can not finish the test or score that you get less than minimum pass, we are sorry can not process your job applicant to the next step.</p>

                        <label class="form-check-label text-muted" style="margin-left: 20px;">
                          <input type="checkbox" class="form-check-input" checked >
                          <p>I agree to all Terms & Conditions</p>
                        </label>
                      </div>

                     

                      <div style="margin-top:30px">
                      <p>Best Regard</p>
                      </div>

                      <div style="margin-top:30px">
                      <p>PT Megah Mas Prima</p>
                      </div>
                  </div>
                </div>
                <?php
                  if(empty($check->result())){?>
                    <a class="filter btn btn-common" style="margin-top:10px" href="<?=base_url("user/applyJob/$idJob")?>">
                        Start Tes Online 
                    </a>
                  <?php }else{ ?>
                    <a class="filter btn btn-common" style="margin-top:10px" href="<?=base_url("user/applyJob/$idJob")?>">
                        Apply Job
                    </a>
                   <?php } ?>
                  

                </div>
              </div>
            </div>

        </div>
      </div>
    </div>
    <!-- Resume Section End -->