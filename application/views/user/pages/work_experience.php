
    <!-- About Section Start -->
    <section id="experience" class="section-padding">
    <h2 class="section-title wow flipInX" data-wow-delay="0.4s">Work Exeperience</h2>

      <div class="container">
        <div class="row">
        <?php if(!empty($work_experience->row())){
          ?>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="wow fadeInRight" data-wow-delay="0.3s">
              <div class="about-profile">
              <table  class="table" >
                        <tr>
                            <td>Instituion Name</td>
                            <td>:</td>
                            <td><?=$work_experience->row()->institutionName;?></td>
                        </tr>
                        <tr>
                            <td>Position</td>
                            <td>:</td>
                            <td><?=$work_experience->row()->position;?></td>
                        </tr>
                        <tr>
                            <td>Start Date</td>
                            <td>:</td>
                            <td><?=$work_experience->row()->startDate;?></td>
                        </tr>
                        <tr>
                            <td>End Date</td>
                            <td>:</td>
                            <td><?=$work_experience->row()->endDate;?></td>
                        </tr>
                        <tr>
                            <td>Job Description</td>
                            <td>:</td>
                            <td><?=$work_experience->row()->jobDescription;?></td>
                        </tr>
                        <tr>
                            <td>Reason for Resign</td>
                            <td>:</td>
                            <td><?=$work_experience->row()->reasonResign;?></td>
                        </tr>
                </table>
              </div>
              <a href="<?=base_url('user/formWorkExperience')?>" class="btn btn-common"><i class="icon-paper-clip"></i> Edit Data</a>
              <a href="<?=base_url('user/deleteWorkExperience/')?>" class="btn btn-danger"><i class="icon-speech"></i> Hapus Data</a>
            </div>
          </div>  
          <?php } else {?>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="justify-content:center;display:flex;flex-direction: column;">
              <h3 style="text-align: center;">Data Is Empty</h3>
              <a href="<?=base_url('user/formWorkExperience')?>" class="btn btn-common"><i class="icon-paper-clip"></i>Add Data</a>
          </div>
        <?php } ?>
        </div>
      </div>
    </section>
    <!-- About Section End -->
