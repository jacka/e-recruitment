
    <!-- About Section Start -->
    <section id="formal" class="section-padding">
    <h2 class="section-title wow flipInX" data-wow-delay="0.4s">Formal Education</h2>

      <div class="container">
        <div class="row">

        <?php if(!empty($formal_education->row())){?>
          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="img-thumb wow fadeInLeft" data-wow-delay="0.3s">
              <img class="img-fluid" src="<?=base_url()?><?=$formal_education->row()->imgCertificate;?>" style="width: 580px;" alt="">
            </div>
          </div> 

          <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="wow fadeInRight" data-wow-delay="0.3s">
              
              <div class="about-profile">

              <table  class="table" >
                        <tr>
                            <td>Education Level</td>
                            <td>:</td>
                            <td><?=$formal_education->row()->educationLevel;?></td>
                        </tr>
                        <tr>
                            <td>Education Institution</td>
                            <td>:</td>
                            <td><?=$formal_education->row()->educationInstitution;?></td>
                        </tr>
                        <tr>
                            <td>Graduation Certificate Series Number</td>
                            <td>:</td>
                            <td><?=$formal_education->row()->certificateSeries;?></td>
                        </tr>
                        <tr>
                            <td>Year of Entry</td>
                            <td>:</td>
                            <td><?=$formal_education->row()->yearEntry;?></td>
                        </tr>
                        <tr>
                            <td>Graduation Years</td>
                            <td>:</td>
                            <td><?=$formal_education->row()->graduationYear;?></td>
                        </tr>
                        <tr>
                            <td>Institution Place</td>
                            <td>:</td>
                            <td><?=$formal_education->row()->institutionPlace;?></td>
                        </tr>
                        <tr>
                            <td>Graduation Certificate Number</td>
                            <td>:</td>
                            <td><?=$formal_education->row()->certificateNumber;?></td>
                        </tr>
                </table>
              </div>
              <!-- <a href="<?=base_url('user/formFormalEducation')?>" class="btn btn-common"><i class="icon-paper-clip"></i>Ubah Data</a> -->
              <a href="<?=base_url('user/deleteFormalEducation/')?>" class="btn btn-danger"><i class="icon-speech"></i>Hapus Data</a>
            </div>
          </div>  
          <?php } else {?>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="justify-content:center;display:flex;flex-direction: column;">
              <h3 style="text-align: center;">Data Is Empty</h3>
              <a href="<?=base_url('user/formFormalEducation')?>" class="btn btn-common"><i class="icon-paper-clip"></i>Add Data</a>
          </div>
        <?php } ?>


        </div>
      </div>
    </section>
    <!-- About Section End -->
