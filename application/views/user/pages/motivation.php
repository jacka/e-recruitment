
    <!-- About Section Start -->
    <section id="motivation" class="section-padding">
    <h2 class="section-title wow flipInX" data-wow-delay="0.4s">Motivation</h2>

      <div class="container">
        <div class="row">
          
        <?php if(!empty($motivation->row())){
          ?>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="wow fadeInRight" data-wow-delay="0.3s">
              <div class="about-profile">

              <table  class="table" >
                        <tr>
                            <td>Why we should chooese you</td>
                            <td>:</td>
                            <td><?=$motivation->row()->reasonChoose;?></td>
                        </tr>
                        <tr>
                            <td>Expertise</td>
                            <td>:</td>
                            <td><?=$motivation->row()->expertise;?></td>
                        </tr>
                        <tr>
                            <td>Expected Sallary</td>
                            <td>:</td>
                            <td><?=$motivation->row()->expectedSallary;?></td>
                        </tr>
                        <tr>
                            <td>Hobby</td>
                            <td>:</td>
                            <td><?=$motivation->row()->hobby;?></td>
                        </tr>
                </table>

              </div>
              <!-- <a href="#" class="btn btn-common"><i class="icon-paper-clip"></i> Ubah Data</a> -->
              <a href="<?=base_url('user/deleteMotivation/')?>" class="btn btn-danger"><i class="icon-speech"></i> Hapus Data</a> 
            </div>
          </div>  
          <?php } else {?>
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="justify-content:center;display:flex;flex-direction: column;">
              <h3 style="text-align: center;">Data Is Empty</h3>
              <a href="<?=base_url('user/formMotivation')?>" class="btn btn-common"><i class="icon-paper-clip"></i>Add Data</a>
          </div>
        <?php } ?>

        </div>
      </div>
    </section>
    <!-- About Section End -->
