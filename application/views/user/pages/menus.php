<section id="services" class="services section-padding">
      <h2 class="section-title wow flipInX" data-wow-delay="0.4s" style="margin-top: 50px;">Curiculum Vitae</h2>
      <div class="flash-data" data-home="<?php echo $this->session->flashdata('item');?>"></div>

      <div class="container">
        <div class="row">

          <!-- Services item -->
          <a href="#personal-data">
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInDown" data-wow-delay="0.3s">
              <div class="icon">
                <i class="icon-grid"></i>
              </div>
              <div class="services-content">
                <h3><a href="#personal-data">Personal Data</a></h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse condi.</p>
              </div>
            </div>
          </div>
          </a>


          <!-- Services item -->
          <a href="#formal">
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInDown" data-wow-delay="0.6s">
              <div class="icon">
                <i class="icon-graduation"></i>
              </div>
              <div class="services-content">
                <h3><a href="#formal">Formal Education</a></h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse condi.</p>
              </div>
            </div>
          </div>
          </a>

          <!-- Services item -->
          <a href="#non-formal">
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInDown" data-wow-delay="0.9s">
              <div class="icon">
                <i class="icon-layers"></i>
              </div>
              <div class="services-content">
                <h3><a href="#non-formal">Non-Formal Education</a></h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse condi.</p>
              </div>
            </div>
          </div>
          </a>

          </div>

          <div class="row">
          <!-- Services item -->
          <a href="#family">
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInDown" data-wow-delay="1.2s">
              <div class="icon">
                <i class="icon-diamond"></i>
              </div>
              <div class="services-content">
                <h3><a href="#family">Family</a></h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse condi.</p>
              </div>
            </div>
          </div>
          </a>

          <!-- Services item -->
          <a href="#experience">
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInDown" data-wow-delay="1.2s">
              <div class="icon">
                <i class="icon-briefcase"></i>
              </div>
              <div class="services-content">
                <h3><a href="#experience">Work Experience</a></h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse condi.</p>
              </div>
            </div>
          </div>
          </a>

          <!-- Services item -->
          <a href="#motivation">
          <div class="col-md-6 col-lg-4 col-xs-12">
            <div class="services-item wow fadeInDown" data-wow-delay="1.2s">
              <div class="icon">
                <i class="icon-eye"></i>
              </div>
              <div class="services-content">
                <h3><a href="#motivation">Motivation</a></h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse condi.</p>
              </div>
            </div>
          </div>
          </a>

        </div>

        <div class="row">
          <!-- Services item -->
          <div class="col-md-12 col-lg-12 col-xs-12">
              <div style="margin-top:20px">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse condi, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse condi, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse condi, Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse condi.</p>
              </div>

              <div style="margin-top:30px">
              <p>Best Regard</p>
              </div>

              <div style="margin-top:30px">
              <p>PT Megah Mas Prima</p>
              </div>
          </div>
        </div>
      </div>

      
    </section>