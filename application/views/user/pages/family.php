
    <!-- About Section Start -->
    <section id="family" class="section-padding">
    <h2 class="section-title wow flipInX" data-wow-delay="0.4s">Family</h2>

      <div class="container">
        <div class="row">
          
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="profile-wrapper wow fadeInRight" data-wow-delay="0.3s">
              <div class="about-profile">
              <table  class="table" >
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Place Of Birth</th>
                            <th>Date Of Birth</th>
                            <th>Status</th>
                            <th>Last Education</th>
                            <th>Profession</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($family->row())){
                       $no=1;
                       foreach($family->result() as $data) {
                      ?>
                        <tr>
                            <td><?=$no;?></td>
                            <td><?=$data->name;?></td>
                            <td><?=$data->placeBirth;?></td>
                            <td><?=$data->dateBirth;?></td>
                            <td><?=$data->status;?></td>
                            <td><?=$data->lastEducation;?></td>
                            <td><?=$data->profession;?></td>
                        </tr>
                    <?php $no++; } }else{ ?>
                      <tr>
                      <td colspan="7" align="center">Data Is Empty</td>
                      </tr>
                    <?php } ?>
                    </tbody>
                </table>
              </div>
              <a href="<?=base_url('user/formFamily')?>" class="btn btn-common"><i class="icon-paper-clip"></i>Tambah Data</a>
              <a href="<?=base_url('user/deleteFamily/')?>" class="btn btn-danger"><i class="icon-speech"></i> Delete Data</a> 
            </div>
          </div>   
        </div>
      </div>
    </section>
    <!-- About Section End -->
