 <!-- Contact Section Start -->
 <section id="contact" class="section-padding">      
      <div class="contact-form">
        <div class="container">
          <div class="row contact-form-area wow fadeInUp" data-wow-delay="0.4s">          
            <div class="col-md-12 col-lg-12 col-sm-12">
              <div class="contact-block">
              <h2 class="section-title wow flipInX" data-wow-delay="0.4s">Family</h2>
              <?php
                   echo form_open('user/formFamily');
                 ?>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                      <label>Name </label> <label style="color:red">*</label>
                        <input type="text" class="form-control" id="name" name="name" placeholder="Name" required data-error="Please enter your name">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>
                    <div class="col-md-6">
                     
                      <div class="form-group">
                      <label> Place Of Birth </label> <label style="color:red">*</label>
                        <input type="text" placeholder="Place Of Birth" id="placeBirth" class="form-control" name="placeBirth" required data-error="Please enter your Place Of Birth">
                        <div class="help-block with-errors"></div>
                      </div> 
                    </div>
                     <div class="col-md-6">
                      <div class="form-group">
                      <label> Date Of Birth </label> <label style="color:red">*</label>
                        <input type="date" placeholder="Date Of Birth" id="dateBirth" name="dateBirth" class="form-control" required data-error="Please enter your Date Of Birth">
                        <div class="help-block with-errors"></div>
                      </div>
                    </div>

                    <div class="col-md-12">
                    <div class="form-group">
                    <label> Status </label> <label style="color:red">*</label>
                  <select class="form-control form-control-lg" id="exampleFormControlSelect2" id="status" name="status" required data-error="Please enter your status">
                    <option>Mother</option>
                    <option>Father</option>
                    <option>Brother</option>
                    <option>Sister</option>
                  </select>
                </div>
                    </div>

                    <div class="col-md-12">
                    <div class="form-group">
                    <label> Last Education </label> <label style="color:red">*</label>
                  <select class="form-control form-control-lg" id="exampleFormControlSelect2" id="lastEducation" name="lastEducation" required data-error="Please enter your status">
                    <option>SLTA</option>
                    <option>Sarjana</option>
                    <option>Magister</option>
                    <option>Doktor</option>
                  </select>
                </div>
                    </div>


                    <div class="col-md-12">
                      <div class="form-group">
                      <label> Profession </label> <label style="color:red">*</label>
                        <input type="text" class="form-control" id="profession" name="profession" placeholder="Profession" required data-error="Please enter your name">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>

                    <div class="col-md-12">
                      <div class="submit-button">
                        <button class="btn btn-common" id="submit" type="submit" name="submit">Submit</button>
                        <div id="msgSubmit" class="h3 text-center hidden"></div> 
                        <div class="clearfix"></div> 
                      </div>
                    </div>
                    </div>
                  </div>            
                </form>
              </div>
            </div>
          
          </div>
        </div>
      </div>   
    </section>
    <!-- Contact Section End -->