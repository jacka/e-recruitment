 <!-- Contact Section Start -->
 <section id="contact" class="section-padding">      
      <div class="contact-form">
        <div class="container">
          <div class="row contact-form-area wow fadeInUp" data-wow-delay="0.4s">          
            <div class="col-md-12 col-lg-12 col-sm-12">
              <div class="contact-block">
              <h2 class="section-title wow flipInX" data-wow-delay="0.4s">Non Formal Education</h2>
              <?php
                  echo form_open_multipart('user/formNonFormalEducation');
                 ?>
                  <div class="row">
                    
                  <div class="col-md-6">
                      <div class="form-group">
                      <label> Training Name </label> <label style="color:red">*</label>
                        <input type="text" class="form-control" id="trainingName" name="trainingName" placeholder="Training Name" required data-error="Please enter your Training Name">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>


                    <div class="col-md-6">
                      <div class="form-group">
                      <label> Training Scope </label> <label style="color:red">*</label>
                        <input type="text" class="form-control" id="trainingScope" name="trainingScope" placeholder="Training Scope" required data-error="Please enter your Training Scope">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                      <label> Training Institution </label> <label style="color:red">*</label>
                        <input type="text" class="form-control" id="trainingInstitution" name="trainingInstitution" placeholder="Training Institution" required data-error="Please enter your Training Institution">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                      <label> Training Certificate Number </label> 
                        <input type="text" class="form-control" id="certificateNumber" name="certificateNumber" placeholder="Training Certificate Number">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>


                    <div class="col-md-6">
                      <div class="form-group">
                      <label>Start Date </label> <label style="color:red">*</label>
                        <input type="date"  id="startDate" class="form-control" name="startDate" required data-error="Please enter your Start Date">
                        <div class="help-block with-errors"></div>
                      </div>
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                      <label>End Date</label> <label style="color:red">*</label>
                        <input type="date" id="endDate" class="form-control" name="endDate" required data-error="Please enter your End Date">
                        <div class="help-block with-errors"></div>
                      </div>
                    </div>

                    <div class="col-md-12">
                    <div class="form-group">
                    <label>Upload Certificate</label> <label style="color:red">*</label>
                    <div class="input-group col-xs-12">
                       <input type="file" name="img" class="form-control file-upload-info">
                    </div>
                  </div>
                    </div>

                  <div class="col-md-12">
                      <div class="submit-button">
                        <button class="btn btn-common" id="submit" type="submit" name="submit">Submit</button>
                        <div id="msgSubmit" class="h3 text-center hidden"></div> 
                        <div class="clearfix"></div> 
                      </div>
                  </div>
                    </div>
                  </div>            
                </form>
              </div>
            </div>
          
          </div>
        </div>
      </div>   
    </section>
    <!-- Contact Section End -->