 <!-- Contact Section Start -->
 <section id="contact" class="section-padding">      
      <div class="contact-form">
        <div class="container">
          <div class="row contact-form-area wow fadeInUp" data-wow-delay="0.4s">          
            <div class="col-md-12 col-lg-12 col-sm-12">
              <div class="contact-block">
              <h2 class="section-title wow flipInX" data-wow-delay="0.4s">Motivation</h2>
              <?php
                  echo form_open('user/formMotivation');
                 ?>
                  <div class="row">
                    <div class="col-md-6">
                     <div class="form-group">
                     <label> Why we should chooese you </label> <label style="color:red">*</label>
                     <textarea class="form-control"  placeholder="Why we should chooese you " name="reasonChoose" rows="5" data-error="Write your message" required></textarea>
                       <div class="help-block with-errors"></div>
                     </div> 
                   </div>

                    <div class="col-md-6">
                     <div class="form-group">
                     <label> Expertise </label> <label style="color:red">*</label>
                     <textarea class="form-control"  placeholder="Expertise" name="expertise" rows="5" data-error="Write your message" required></textarea>
                       <div class="help-block with-errors"></div>
                     </div>
                   </div>

                   <div class="col-md-6">
                      <div class="form-group">
                      <label> Expected Sallary </label> <label style="color:red">*</label>
                        <input type="text" class="form-control" id="expectedSallary" name="expectedSallary" placeholder="Expected Sallary" required data-error="Please enter your name">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                      <label> Hobby </label> <label style="color:red">*</label>
                        <input type="text" class="form-control" id="hobby" name="hobby" placeholder="Hobby" required data-error="Please enter your name">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>

                    <div class="col-md-12">
                     
                     <div class="form-group">
                     <label> Motivation </label> <label style="color:red">*</label>
                     <textarea class="form-control"  placeholder="Motivation" name="motivation" rows="5" data-error="Write your message" required></textarea>
                       <div class="help-block with-errors"></div>
                     </div> 
                   </div>


                    <div class="col-md-12">
                      <div class="submit-button">
                        <button class="btn btn-common" id="submit" type="submit" name="submit">Submit</button>
                        <div id="msgSubmit" class="h3 text-center hidden"></div> 
                        <div class="clearfix"></div> 
                      </div>
                    </div>

                    </div>
                  </div>            
                </form>
              </div>
            </div>
          
          </div>
        </div>
      </div>   
    </section>
    <!-- Contact Section End -->