 <!-- Contact Section Start -->
 <section id="contact" class="section-padding">      
      <div class="contact-form">
        <div class="container">
          <div class="row contact-form-area wow fadeInUp" data-wow-delay="0.4s">          
            <div class="col-md-12 col-lg-12 col-sm-12">
              <div class="contact-block">
              <h2 class="section-title wow flipInX" data-wow-delay="0.4s">Work Experience</h2>
              <?php
                  echo form_open('user/formWorkExperience');
                 ?>
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group">
                      <label> Instituion Name </label> <label style="color:red">*</label>
                        <input type="text" class="form-control" id="instituionName" name="institutionName" placeholder="Instituion Name" required data-error="Please enter your Instituion Name">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>

                    <div class="col-md-6">
                      <div class="form-group">
                      <label> Position </label> <label style="color:red">*</label>
                        <input type="text" class="form-control" id="position" name="position" placeholder="Position" required data-error="Please enter your name">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>


                    <div class="col-md-6">
                     
                      <div class="form-group">
                      <label> Start Year </label> <label style="color:red">*</label>
                        <select class="form-control form-control-lg" id="startDate" name="startDate" required>
                            <option>2019</option>
                            <option>2018</option>
                            <option>2017</option>
                            <option>2016</option>
                            <option>2015</option>
                            <option>2014</option>
                            <option>2013</option>
                            <option>2012</option>
                            <option>2011</option>
                            <option>2010</option>
                            <option>2009</option>
                            <option>2008</option>
                            <option>2007</option>
                            <option>2006</option>
                            <option>2005</option>
                            <option>2004</option>
                            <option>2003</option>
                            <option>2002</option>
                            <option>2001</option>
                            <option>2000</option>
                        </select>
                        <div class="help-block with-errors"></div>
                      </div> 
                    </div>

                     <div class="col-md-6">
                      <div class="form-group">
                      <label> End Year </label> <label style="color:red">*</label>
                        <select class="form-control form-control-lg" id="endDate" name="endDate" required>
                            <option>2019</option>
                            <option>2018</option>
                            <option>2017</option>
                            <option>2016</option>
                            <option>2015</option>
                            <option>2014</option>
                            <option>2013</option>
                            <option>2012</option>
                            <option>2011</option>
                            <option>2010</option>
                            <option>2009</option>
                            <option>2008</option>
                            <option>2007</option>
                            <option>2006</option>
                            <option>2005</option>
                            <option>2004</option>
                            <option>2003</option>
                            <option>2002</option>
                            <option>2001</option>
                            <option>2000</option>
                        </select>
                        <div class="help-block with-errors"></div>
                      </div>
                    </div>

                    <div class="col-md-6">
                     
                     <div class="form-group">
                     <label> Job Description </label> <label style="color:red">*</label>
                     <textarea class="form-control"  placeholder="Job Description" name="jobDescription" rows="5" data-error="Write your message" required></textarea>
                       <div class="help-block with-errors"></div>
                     </div> 
                   </div>

                    <div class="col-md-6">
                     <div class="form-group">
                     <label> Reason for Resign </label> <label style="color:red">*</label>
                     <textarea class="form-control"  placeholder="Reason for Resign" rows="5" name="reasonResign" data-error="Write your message" required></textarea>
                       <div class="help-block with-errors"></div>
                     </div>
                   </div>

                    <div class="col-md-12">
                      <div class="submit-button">
                        <button class="btn btn-common" id="submit" type="submit" name="submit">Submit</button>
                        <div id="msgSubmit" class="h3 text-center hidden"></div> 
                        <div class="clearfix"></div> 
                      </div>
                    </div>

                    </div>
                  </div>            
                </form>
              </div>
            </div>
          
          </div>
        </div>
      </div>   
    </section>
    <!-- Contact Section End -->