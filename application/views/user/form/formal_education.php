 <!-- Contact Section Start -->
 <section id="contact" class="section-padding">      
      <div class="contact-form">
        <div class="container">
          <div class="row contact-form-area wow fadeInUp" data-wow-delay="0.4s">          
            <div class="col-md-12 col-lg-12 col-sm-12">
              <div class="contact-block">
              <h2 class="section-title wow flipInX" data-wow-delay="0.4s">Formal Education</h2>
              <?php
                  echo form_open_multipart('user/formFormalEducation');
                 ?>
                  <div class="row">
                    
                  <div class="col-md-12">
                    <div class="form-group">
                    <label> Education Level </label> <label style="color:red">*</label>
                  <select class="form-control form-control-lg" id="educationLevel" name="educationLevel" required data-error="Please enter your Education Level">
                    <option>SLTA</option>
                    <option>Sarjana</option>
                    <option>Magister</option>
                    <option>Doktor</option>
                  </select>
                </div>
                    </div>


                    <div class="col-md-12">
                      <div class="form-group">
                      <label> Education Institution </label> <label style="color:red">*</label>
                        <input type="text" class="form-control" id="educationInstitution" name="educationInstitution" placeholder="educationInstitution" required data-error="Please enter your Education Institution">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                      <label> Graduation Certificate Series Number </label> 
                        <input type="text" class="form-control" id="certificateSeries" name="certificateSeries" placeholder="certificateSeries">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>

                    <div class="col-md-6">  
                      <div class="form-group">
                      <label>Year of Entry</label> <label style="color:red">*</label>
                      <select class="form-control form-control-lg" id="yearEntry" name="yearEntry" required>
                      <option>2019</option>
                      <option>2018</option>
                      <option>2017</option>
                      <option>2016</option>
                      <option>2015</option>
                      <option>2014</option>
                      <option>2013</option>
                      <option>2012</option>
                      <option>2011</option>
                      <option>2010</option>
                      <option>2009</option>
                      <option>2008</option>
                      <option>2007</option>
                      <option>2006</option>
                      <option>2005</option>
                      <option>2004</option>
                      <option>2003</option>
                      <option>2002</option>
                      <option>2001</option>
                      <option>2000</option>
                  </select>
                        <div class="help-block with-errors"></div>
                      </div> 
                    </div>
                     <div class="col-md-6">
                      <div class="form-group">
                      <label> Gradution Years </label> <label style="color:red">*</label>
                      <select class="form-control form-control-lg" id="graduationYear" name="graduationYear" required>
                      <option>2019</option>
                      <option>2018</option>
                      <option>2017</option>
                      <option>2016</option>
                      <option>2015</option>
                      <option>2014</option>
                      <option>2013</option>
                      <option>2012</option>
                      <option>2011</option>
                      <option>2010</option>
                      <option>2009</option>
                      <option>2008</option>
                      <option>2007</option>
                      <option>2006</option>
                      <option>2005</option>
                      <option>2004</option>
                      <option>2003</option>
                      <option>2002</option>
                      <option>2001</option>
                      <option>2000</option>
                  </select>
                        <div class="help-block with-errors"></div>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                      <label> Institution Place </label> <label style="color:red">*</label>
                        <input type="text" class="form-control" id="institutionPlace" name="institutionPlace" placeholder="institution place" required data-error="Please enter your institution place">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                      <label> Gradution Certificate Number </label><label style="color:red">*</label>
                        <input type="text" class="form-control" id="certificateNumber" name="certificateNumber" placeholder="graduation certificate" required data-error="Please enter your certificate number">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>

                      <div class="form-group">
                    <label>Gradution Certificate</label> <label style="color:red">*</label>
                    <div class="input-group col-xs-12">
                       <input type="file" name="img" class="form-control file-upload-info">
                    </div>
                  </div>

                  <div class="col-md-12">
                      <div class="submit-button">
                        <button class="btn btn-common" id="submit" type="submit" name="submit">Submit</button>
                        <div id="msgSubmit" class="h3 text-center hidden"></div> 
                        <div class="clearfix"></div> 
                      </div>
                  </div>
                    </div>
                  </div>            
                </form>
              </div>
            </div>
          
          </div>
        </div>
      </div>   
    </section>
    <!-- Contact Section End -->