 <!-- Contact Section Start -->
 <section id="contact" class="section-padding">      
      <div class="contact-form">
        <div class="container">
          <div class="row contact-form-area wow fadeInUp" data-wow-delay="0.4s">          
            <div class="col-md-12 col-lg-12 col-sm-12">
              <div class="contact-block">
              <h2 class="section-title wow flipInX" data-wow-delay="0.4s">Personal Data</h2>
                <!-- <form id="contactForm"> -->
                <?php
                  echo form_open_multipart('user/formPersonalData');
                 ?>
                  <div class="row">
                    <div class="col-md-12">
                      <div class="form-group">
                      <label> Full Name </label> <label style="color:red">*</label>
                        <input type="text" class="form-control" id="fullName" name="fullName" placeholder="Full Name" required data-error="Please enter your name">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>
                    <div class="col-md-6">
                     
                      <div class="form-group">
                      <label> Place Of Birth </label> <label style="color:red">*</label>
                        <input type="text" placeholder="place Of birth" id="placeBirth" class="form-control" name="placeBirth" required data-error="Please enter your Place Of Birth">
                        <div class="help-block with-errors"></div>
                      </div> 
                    </div>
                     <div class="col-md-6">
                      <div class="form-group">
                      <label> Date Of Birth </label> <label style="color:red">*</label>
                        <input type="date" placeholder="date Of birth" id="dateBirth" class="form-control" name="dateBirth" required data-error="Please enter your Date Of Birth">
                        <div class="help-block with-errors"></div>
                      </div>
                    </div>

                    <div class="col-md-12">
                    <div class="form-group">
                    <label> Gender </label> <label style="color:red">*</label>
                  <select class="form-control form-control-lg" id="gender" name="gender" required data-error="Please enter your gender">
                    <option>Male</option>
                    <option>Female</option>
                  </select>
                </div>
                    </div>

                    <div class="col-md-6">
                     
                      <div class="form-group">
                      <label> Height (cm) 
                        <input type="text" placeholder="Height" class="form-control" id="height" name="height" required data-error="Please enter your height">
                        <div class="help-block with-errors"></div>
                      </div> 
                    </div>
                     <div class="col-md-6">
                      <div class="form-group">
                      <label> Weight (kg) </label> 
                      <input type="text" placeholder="weight" class="form-control" id="weight" name="weight" required data-error="Please enter your weight">
                        <div class="help-block with-errors"></div>
                      </div>
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                      <label> Religion </label> <label style="color:red">*</label>
                        <input type="text" class="form-control" id="religion" name="religion" placeholder="Religion" required data-error="Please enter your religion">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>

                    <div class="col-md-12">
                      <div class="form-group">
                      <label> Phone Number </label><label style="color:red">*</label>
                        <input type="text" class="form-control" id="phoneNumber" name="phoneNumber" placeholder="phone number" required data-error="Please enter your Phone Number">
                        <div class="help-block with-errors"></div>
                      </div>                                 
                    </div>

                    <div class="col-md-12">
                      <div class="form-group"> 
                      <label> Address </label> <label style="color:red">*</label>
                        <textarea class="form-control" id="address" name="address"  placeholder="Address" rows="5" data-error="Write your address" required></textarea>
                        <div class="help-block with-errors"></div>
                      </div>

                      <div class="form-group">
                    <label>Photo Profile</label> <label style="color:red">*</label>
                    <div class="input-group col-xs-12">
                       <input type="file" name="img" class="form-control file-upload-info" data-error="Upload your image" required>
                    </div>
                  </div>

                  
                      <div class="submit-button">
                        <button class="btn btn-common" id="submit" type="submit" name="submit">Submit</button>
                        <div id="msgSubmit" class="h3 text-center hidden"></div> 
                        <div class="clearfix"></div> 
                      </div>
                    </div>
                  </div>            
                </form>
              </div>
            </div>
          
          </div>
        </div>
      </div>   
    </section>
    <!-- Contact Section End -->