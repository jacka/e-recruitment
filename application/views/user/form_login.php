<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>E-Recruitment</title>
  <!-- plugins:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>vendors/iconfonts/mdi/css/materialdesignicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>vendors/css/vendor.bundle.base.css">
  <!-- endinject -->
  <!-- plugin css for this page -->
  <!-- End plugin css for this page -->
  <!-- inject:css -->
  <link rel="stylesheet" href="<?php echo base_url('assets/admin/') ?>css/style.css">
  <!-- endinject -->
  <link rel="shortcut icon" href="<?php echo base_url('assets/') ?>logo.png" />
</head>

<body>
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-center auth">
        <div class="row w-100">
          <div class="col-lg-4 mx-auto">
            <div class="auth-form-light text-left p-5">
              <div class="brand-logo">
                <img src="<?php echo base_url('assets/') ?>logo.png">
              </div>
              <h4>Login</h4>
              <div class="flash-data" data-login-user="<?php echo $this->session->flashdata('item'); ?>"></div>
              <!-- <form class="pt-3"> -->
              <?php
                  echo form_open('user/login');
                 ?>
                <div class="form-group">
                <input type="email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Email" name="email" required>
                </div>
                <div class="form-group">
                <input type="password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password" name="password" required>
                </div>
                <div class="mb-4">
                  <div class="form-check">
                    <a href="<?php echo base_url('user/forgotPassword'); ?>" class="form-check-label text-muted">
                      Forgot Password
                    </a>
                  </div>
                </div>
                <div class="mt-3">
                  <!-- <a class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" href="../../index.html">SIGN UP</a> -->
                  <button  class="btn btn-block btn-gradient-primary btn-lg font-weight-medium auth-form-btn" type="submit" name="submit">Login</button>
                </div>
                <div class="text-center mt-4 font-weight-light">
                  Don't have account <a href="<?php echo base_url('user/register'); ?>" class="text-primary">Register</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="<?php echo base_url('assets/admin/') ?>vendors/js/vendor.bundle.base.js"></script>
  <script src="<?php echo base_url('assets/admin/') ?>vendors/js/vendor.bundle.addons.js"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="<?php echo base_url('assets/admin/') ?>js/off-canvas.js"></script>
  <script src="<?php echo base_url('assets/admin/') ?>js/misc.js"></script>
  <!-- endinject -->
  <script type="text/javascript" src="<?php echo base_url('assets/admin') ?>/js/admin.js"></script>
  
 <!-- js custom admin -->                                                                       
 <script type="text/javascript" src="<?php echo base_url('assets/admin/js/sweetalert/') ?>sweetalert2.all.min.js"></script>
 <!-- tutup custom admin js -->
</body>

</html>
